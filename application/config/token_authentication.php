<?php defined('BASEPATH') OR exit('No direct script access allowed');

//cookie information
$cookieName = "SOTHEBY_AUTH";     // just a string for cookie identification just use a string,
//apparently "."s get converted to "_"s so ya I am not sure what else gets changed
$domainPath = "localhost";    // this is used in the cookie to determine what domain have access to the cookie

//token security settings
$tokenPadLength = 24;                       // must be between 18 and 35
$baseConvertTimestampValue = 16;            // must be between 8 and 20
$baseConvertRandomValue = 16;               // must be between 8 and 20
$baseConvertUserIDValue = 16;               // must be between 8 and 20
$baseConvertIPAddressValue = 16;            // must be between 8 and 20
$tokenSalt = "dfdflkdfajdskfksdfjadsfdldlrrtttttyyyyyy";    //must be random length > 20 characters
$maximumIdleTime = 7200;                     //time in seconds