<?php

class DTOMapper
{
	public static function mapUser (\Entity\User $user)
	{
		$dto = new stdClass();
		$dto ->Id = $user -> getId();
		$dto ->Username = $user -> getUserName();
		$dto ->FirstName = $user -> getFirstName();
		$dto ->LastName = $user -> getLastName();
		$dto ->Email = $user -> getEmail();
		$dto ->ProfilePhotos = $user -> getProfilePhotos();
		$dto ->Registered = $user -> getRegistered();
		$dto ->LastUpdated = $user -> getLastUpdated();
		$dto ->LastLoggedOn = $user -> getLastLoggedOn();
		$dto ->IsActivated = $user -> getIsActivated();
		$dto ->Group = DTOMapper::mapUserGroup($user -> getGroup());
		return $dto;

	}
	
	public static function mapUserGroup (\Entity\UserGroup $group)
	{
		$dto = new stdClass();
		$dto ->Id = $group -> getId();
		$dto ->Name = $group -> getName();
		return $dto;
	}
}
