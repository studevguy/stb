<?php
/**
 * Created by IntelliJ IDEA.
 * User: tiger
 * Date: 06/10/2014
 * Time: 15:14
 */

class NumberOfVisitsRevisitsPerExhibition {
    protected $name;
    protected $visits;
    protected $revisits;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRevisits()
    {
        return $this->revisits;
    }

    /**
     * @param mixed $revisits
     */
    public function setRevisits($revisits)
    {
        $this->revisits = $revisits;
    }

    /**
     * @return mixed
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * @param mixed $visits
     */
    public function setVisits($visits)
    {
        $this->visits = $visits;
    }

} 