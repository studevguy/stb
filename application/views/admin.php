<!DOCTYPE html>
<html>
    <head>
        <title>Sothebys</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="images/favicon.png" rel="icon" type="image/png"/>
        <link href="Styles/bootstrap.css" rel="stylesheet" />
        <link href="Styles/bootstrap-theme.css" rel="stylesheet" />
        <link href="Styles/toastr.min.css" rel="stylesheet"/>
        <link href="Styles/style.css" rel="stylesheet" />
        <link href="Styles/admin/style.css" rel="stylesheet" />
    </head>
<body>
<div>
    <img class="loading-page" style="position:absolute;top:50%;left:50%;width:30px;height:30px;z-index:1200;margin-top:-10px;margin-left:-10px"
         src="images/ajax-loader.gif" />
    <div id="header-logo" class="">
        <a class="pull-left" href="./main">
            <img alt="logo" class="img-responsive" src="images/Sotheby_Logo_white.png" />
        </a>
        <div class="logout">
            <a href="./login/logout">
                <img alt="..." src="images/logout.png"/>
            </a>
        </div>
    </div>
    <div>
        <div class="container">
			<div ng-view></div
         </div>

    </div>
</div>
    <!-- /main content -->
    <!-- build:js scripts/libs.js -->
    <script type="text/javascript" src="Scripts/formatNumber.js"></script>
    <script type="text/javascript" src="Scripts/stringPrototype.js"></script>
    <script type="text/javascript" src="Scripts/linq.js"></script>
    <script type="text/javascript" src="Scripts/underscore.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.9.2.js"></script>
	<script type="text/javascript" src="Scripts/bootbox.min.js"></script>
    <script type="text/javascript" src="Scripts/toastr.js"></script>
    <script type="text/javascript" src="Scripts/angular.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap-select.js"></script>
    <!-- /build -->

    <script type="text/javascript" src="Scripts/require.js"></script>
    
    <script type="text/javascript">
        define('angular', function () {
            return angular;
        });
        var debugMode = true;
        var baseUrl = debugMode ? "admin" : "publishedAdmin";
        require(['require', baseUrl + "/config-require"], function (require, config) {
            // update global require config
            window.require.config(config);

            // load app
            require(['main']);
        });
    </script>
</html>