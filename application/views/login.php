﻿<html>
	<head>
		<title>Sothebys - Login</title>
        <link href="<?php echo base_url();?>/images/favicon.png" rel="icon" type="image/png"/>
		<link href="<?php echo base_url();?>/Styles/bootstrap.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>/Styles/bootstrap-theme.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>/Styles/style.css" rel="stylesheet" />
	</head>
	<body class="login">
		<div class="container">
			<div class="logo">
				<a href="#">
					<img alt="logo" src="<?php echo base_url();?>/images/Sotheby_s_logo.png" />
				</a>
			</div>
			<div class="content col-sm-6 col-sm-offset-3  ">
				<div class="login-wrapper">
                    <form class="form-horizontal" role="form"  name="loginForm" action="<?php echo site_url('login/validate?previousctrl='.$previous_Ctrl) ?>" method="POST">
                        <div class="form-group row">
                            <label for="Username" class="col-sm-4 control-label">Username:</label>
                            <div class="col-sm-8 ">
                                <input type="text" class="form-control"  placeholder="Username" name="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password" class="col-sm-4 control-label">Password:</label>
                            <div class="col-sm-8">
                                <input type="Password" class="form-control" id="inputPassword3" placeholder="Password" name="Password">
                            </div>
                        </div>
                        <div class="error pull-right <?php if(!isset($is_login_fail)){
                            echo "hidden";}?>" role="alert">
                                Username or password are incorrect
                         </div>
                        <div class="clearfix"></div>
                        <br>
                        <button class="btn btn-lg btn-primary radius-3"	type="submit" name=btnSubmit >
                            LOG IN
                        </button>
                    </form>

				</div>
            </div>
            <div class="box-shadow col-sm-6 col-sm-offset-3"></div>
		</div>
	</body>

	<script src="<?php echo base_url();?>/Scripts/jquery-1.9.1.js"></script>
	<script src="<?php echo base_url();?>/Scripts/bootstrap.js"></script>

</html>


