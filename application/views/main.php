<!DOCTYPE html>
<html>
    <head>
        <title>Sothebys</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="images/favicon.png" rel="icon" type="image/png"/>
        <link href="Styles/bootstrap.css" rel="stylesheet" />
        <link href="Styles/bootstrap-theme.css" rel="stylesheet" />
        <link href="Styles/rgraph/animations.css" rel="stylesheet" />
        <link href="Styles/rgraph/ModalDialog.css" rel="stylesheet" />
        <link href="Styles/rgraph/website.css" rel="stylesheet" />
        <link href="Styles/rgraph/website.css" rel="stylesheet" />
        <link href="Styles/toastr.min.css" rel="stylesheet"/>
        <link href="Styles/ngDialog/ngDialog.min.css" rel="stylesheet"/>
        <link href="Styles/ngDialog/ngDialog-theme-default.min.css" rel="stylesheet"/>
        <link href="Styles/bootstrap-select.css" rel="stylesheet" />
        <link href="Styles/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
        <link href="Styles/ngDatePicker.css" rel="stylesheet" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

        <![endif]-->
        <script src="Scripts/lib/modernizr.js"></script>
        <link href="Styles/style.css" rel="stylesheet" />
    </head>
<body>
<div id="container" class="main-content">
    <img class="loading-page" style="position:absolute;top:50%;left:50%;width:30px;height:30px;z-index:1200;margin-top:-10px;margin-left:-10px"
         src="images/ajax-loader.gif" />
    <div ng-view></div>
</div>
    <!-- /main content -->
    <!-- build:js scripts/libs.js -->
    <script type="text/javascript" src="Scripts/formatNumber.js"></script>
    <script type="text/javascript" src="Scripts/stringPrototype.js"></script>
    <script type="text/javascript" src="Scripts/linq.js"></script>
    <script type="text/javascript" src="Scripts/underscore.js"></script>
    <script type="text/javascript" src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="Scripts/jquery-ui-1.9.2.js"></script>
    <script type="text/javascript" src="Scripts/toastr.js"></script>
    <script type="text/javascript" src="Scripts/i18next-1.7.1.js"></script>
    <script type="text/javascript" src="Scripts/angular.js"></script>
    <script type="text/javascript" src="Scripts/ng-i18next/ng-i18next.js"></script>
    <script type="text/javascript" src="Scripts/ngJvectorMap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="Scripts/ngJvectorMap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="Scripts/jquery.screwdefaultbuttonsV2.js"></script>
    <script type="text/javascript" src="Scripts/moment.js"></script>

    <script src="Scripts/libraries/RGraph.common.core.js"></script>
    <script src="Scripts/libraries/RGraph.common.tooltips.js"></script>
    <script src="Scripts/libraries/RGraph.common.dynamic.js"></script>
    <script src="Scripts/libraries/RGraph.common.effects.js"></script>
    <script src="Scripts/jquery.actual.js"></script>
    <script src="Scripts/jquery.tinyscrollbar.js"></script>
    <script src="Scripts/libraries/RGraph.hbar.js"></script>
    <script src="Scripts/libraries/gauge-edited.js"></script>
    <!--<script src="Scripts/horizontalbar.js"></script>-->
    <script src="Scripts/bootstrap-select.js"></script>
    <!-- /build -->

    <script type="text/javascript" src="Scripts/require.js"></script>
    
    <script type="text/javascript">
        define('angular', function () {
            return angular;
        });
        var debugMode = true;
        var baseUrl = debugMode ? "App" : "published";
        require(['require', baseUrl + "/config-require"], function (require, config) {
            // update global require config
            window.require.config(config);

            // load app
            require(['main']);
        });
    </script>
</body>
</html>