<?php

require APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH . 'services/SaleService.php';

class Exhibitions extends  REST_Controller
{

    private $salesService;

    function __construct(){
        parent::__construct();
        $this->salesService = new SalesService();
    }

    function index_get()
    {
        try{

            $result = $this -> salesService ->getExhibitionList();

            $this ->response($result, 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }

    }
    
    function popularlotsbysales_get($id=null){
        try{

            $result = $this ->salesService ->getPopularLotsBySales($id);

            if(!$result){
                $this->response('Not Found', 404);
            }

            $this->response($result, 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }



    }
    
}
