<?php

require APPPATH.'/libraries/REST_Controller.php';

class PopularDepartment extends  REST_Controller
{
    function index_get()
    {
        $mockup_data = array("Impressionist & Modern Art"=>"4200000",
        "Contemporary"=>"3900000",
        "Old Master Paintings"=>"2700000",
        "19th Century"=>"2600000",
        "Prints"=>"1300000",
        "Photography"=>"1100000",
        "Russian Art"=>"700000",
        "British Paintings"=>"200000",
        );                          
        $this->response($mockup_data, 200);
    }
}