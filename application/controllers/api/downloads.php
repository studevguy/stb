<?php

require APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH . 'services/SaleService.php';

class downloads extends REST_Controller {

    private $salesService;

    function __construct(){
        parent::__construct();
        $this->salesService = new SalesService();
    }

    function index_get() {
        try{

            $this->response($this ->salesService ->getTotalDownloads(), 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }

    }
}
?>