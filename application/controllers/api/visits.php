 <?php

require APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH . 'services/SaleService.php';


class visits extends REST_Controller {

    private $salesService;

    private $start_date_filter;

    private $end_date_filter;

    function __construct(){
        parent::__construct();
        $this->salesService = new SalesService();
        $this ->start_date_filter = $this -> _args['startdate'];
        $this ->end_date_filter = $this -> _args['enddate'];
    }

	function exhibitions_get() {
        try{

            $result = $this ->salesService ->getNumberOfVisitsRevisitsPerExhibition($this ->start_date_filter, $this ->end_date_filter);

            $this->response($result, 200);


        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }

	}
    
    
    public function country_get($original){

        try{

            if (!$original){
                $this ->response('Bad Request', 400);
            }

            $result = $this ->salesService ->getVisitPerExhibitionByCountry($original);

            if(!$result){
                $this ->response('Not Found', 404);
            }

            $this->response($result, 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }


    }
}
?>