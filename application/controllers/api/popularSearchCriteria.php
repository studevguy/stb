<?php

require APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH . 'services/SaleService.php';

class PopularSearchCriteria extends  REST_Controller
{
    private $salesService;

    function __construct(){
        parent::__construct();
        $this->salesService = new SalesService();
    }

    public function index_get()
    {
        try{

            $result = $this ->salesService ->getPopularSearchCriteriaData();

            $this -> response($result, 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }
    }
}