 <?php

require APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH . 'services/SaleService.php';

 class TimeSpent extends REST_Controller {
    private  $salesService;

    function  __construct(){
        parent::__construct();
        $this ->salesService = new SalesService();
    }

	function apps_get() {
        try{

            $result = $this ->salesService ->getTimeSpentForPlatform();

            $this -> response($result, 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }
	}


    function pages_get() {

        try{

            $result = $this -> salesService ->getTimeSpentForPage();

            $this -> response($result, 200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server Error", 500);

        }

	}
}
?>