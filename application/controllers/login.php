<?php
/**
 * User Controller
 * This controller fully demonstrates the user class.
 *
 * @package User
 * @author Waldir Bertazzi Junior
 * @link http://waldir.org/
 **/

class Login extends CI_Controller {

    protected $previous_Ctrl;
    protected $cookieName;
    protected $expiredTime;

	function __construct(){
		    parent::__construct();
		    // Load the Library
			$this->load->library('userauthentication');
			$this->load->helper('url');
            require (APPPATH.'/config/token_authentication.php');
            $this ->cookieName = $cookieName;
	}
	
	function index()
	{
 		//If user is already logged in, send it to private page
		if ($this->userauthentication->on_valid_authentication()){
            redirect('main');
        }

        if($this ->input -> get('previousctrl') !=null){
	       $this ->previous_Ctrl = $this ->input -> get('previousctrl');
	    }
        else{
	       $this ->previous_Ctrl =  'main';
	       }
        
        $data['previous_Ctrl'] = $this ->previous_Ctrl;
		
		// Loads the login view
		$this->load->view('login', $data);
 	}
	

	function validate()
	{
	   $this ->previous_Ctrl = $_GET['previousctrl'];
     		// Receives the login data
		$username = $this->input->post('Username');
		$password = $this->input->post('Password');
        $data['previous_Ctrl'] = $this ->previous_Ctrl;
 		
		/* 
		 * Validates the user input
		 * The user->login returns true on success or false on fail.
		 * It also creates the user session.
		*/
		if($this->userauthentication->login($username, $password)){
			// Success
            $expire = time()+3600;
            $token = $this ->userauthentication ->createAuthenticationToken($username);
            $this ->input ->set_cookie($this ->cookieName, $token, $expire);
			redirect($this ->previous_Ctrl);
		} else {
            $data['is_login_fail'] = true;
			// Oh, holdon sir.
			$this ->load ->view('login', $data);
		}
	}
	
	// Simple logout function
	function logout()
	{
		// Removes user authentication cookie and redirects to login
        $this ->input ->set_cookie($this ->cookieName,'');
		redirect('login');
	}}
?>
