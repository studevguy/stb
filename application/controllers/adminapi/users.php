<?php

require APPPATH.'/libraries/REST_Controller.php';
require_once APPPATH.'/libraries/Validator.php';
require_once APPPATH.'/services/userService.php';
require_once APPPATH.'/services/userGroupService.php';


class Users extends  REST_Controller
{

    function __construct(){
        parent::__construct();
        if (!$this -> isAuthenticated){
            $this ->response('Not allowed',405);
        }
    }

	function index_get()
	{
        try{

            $users = UserService::getAllUsers();
            if ($users !=null){
                $this ->response($users,200);
            }
            else{
                $this -> response ("not found", 404);
            }

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("Server error", 500);

        }

	}
	
	function user_get($id){
        try{

            $user = UserService::getUserById($id);
            if ($user !=null){
                $this -> response($user, 200);
            }
            else{
                $this -> response ("not found", 404);
            }

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("erver error", 500);

        }
	}

	function user_post(){

        $v = $this ->getValidator($this -> _post_args, true);

        if(!$v ->validate()){
            $errors = $v ->errors();
            $message = '';

            foreach($errors as $error){
                foreach($error as $err){
                    $message = $message.$err."; ";
                }
            }

            $this -> response ($message, 400);
        }

        try{

            UserService::addUser($this -> _post_args);

            $this ->response (201);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("edit user fail", 500);

        }
	}
    
    function user_put($id){

        $v = $this ->getValidator($this ->_put_args);

        if(!$v ->validate()){
            $errors = $v ->errors();
            $message = '';
            foreach($errors as $error){
                foreach($error as $err){
                    $message = $message.$err."; ";
                }
            }
            $this ->response($message, 400);
        }

        try{

            UserService::updateUser($id,$this -> _put_args);
            $this ->response(200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("update user fail", 500);

        }

    }

    function user_delete($id){
        try{

            UserService::deleteUser($id);
            $this ->response(200);

        }catch (Exception $ex){

            log_message('error', $ex ->getMessage());
            $this ->response("user was not deleted", 500);

        }
    }
	
	private function getValidator($validatingArray, $isCreateRequest = false){
        $v = new \Valitron\Validator($validatingArray);

        $requiredFieldArray = array('Username', 'groupId');
        if($isCreateRequest){
            array_push($requiredFieldArray,'Password');
            array_push($requiredFieldArray,'Passconf');
        }
        $v -> rule('required', $requiredFieldArray);

        $v -> rule('email', 'Email');
        $v -> rule('integer', 'groupId');

        if(array_key_exists('Password', $validatingArray)) {
            $v ->rule('equals','Password','Passconf');
        }

        if(array_key_exists('Passconf', $validatingArray)){
            $v ->rule('lengthMin', ['Password', 'Passconf'], 5);
        }

        return $v;
    }
}
	
