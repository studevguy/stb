<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminDashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        if (file_exists(APPPATH.'/libraries/userauthentication.php')){
            $this->load->library('userAuthentication');
        }
		$this->load->helper('url');
	}

	function index()
	{
        //$current_Ctrl = $this ->router ->class;
        //if (file_exists(APPPATH.'/libraries/userauthentication.php')){
            //if($this->userauthentication->on_invalid_authentication()){
                //redirect('login?previousctrl='.$current_Ctrl);
            //}
        //}
		 $this->load->view('admin');
	}
	
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
