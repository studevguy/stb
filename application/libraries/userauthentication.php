<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Codeigniter User Class
 *
 * @author		Waldir Bertazzi Junior
 * @link 		http://waldir.org/
 */

/*
* This constant is used to make the login 
* keep the last login on the database.
*/
define('DONT_UPDATE_LOGIN', false);
define('PASSWORD_IS_HASHED', true);
define('PASSWORD_IS_NOT_HASHED', false);

require_once APPPATH . '/services/userService.php';

class UserAuthentication {

    /**
     * User Data - This variable holds all user data after session validation
     *
     * @var array
     */
    public $user_data			= array();
    public $custom_data			= array();
    public $user_permission		= array();
    private $use_custom_fields	= true;
    private $CI;
    private $encrypt;

    /**
     * Constructor
     *
     * Loads the session and crypt library.
     * Also gets a instance of CI class.
     */
    function __construct(){
        $this->CI =& get_instance();
        $this  ->encrypt = new CI_Encrypt();

        // load session and bcrypt library.
        $this->CI->load->library(array('Session'));

        // autoloads the user
        //$this->validate_session();
    }

    function login($username, $password, $update_last_login = true, $hashed_password = false){

        $user = UserService::findUser($username);

        if ($user == null){
            return false;
        }

        if ($this ->match_password($password, $user -> getPasswordHashed())){
            return true;
        }
        return false;

    }

    function match_password($login_password, $user_password){
        return $this ->encrypt ->decode($user_password)==$login_password;
    }

    public function createAuthenticationToken($userID){

        if($userID !=null){
            require (APPPATH.'/config/token_authentication.php');
            //get user IP address
            $userIPAddress = ip2long($_SERVER["REMOTE_ADDR"]);

            //convert to a different base
            $timestampBase = base_convert(time(),10, $baseConvertTimestampValue);
            $randomBase = base_convert(rand(0,9999999999),10,$baseConvertRandomValue);
            $userIDBase = base_convert($userID,10,$baseConvertUserIDValue);
            $userIPAddressBase = base_convert($userIPAddress,10,$baseConvertIPAddressValue);

            $timestampBasePadded = str_pad($timestampBase, $tokenPadLength, "0", STR_PAD_LEFT);
            $randomBasePadded = str_pad($randomBase, $tokenPadLength, "0", STR_PAD_LEFT); //just used as filler
            $userIDBasePadded = str_pad($userIDBase, $tokenPadLength, "0", STR_PAD_LEFT);
            $userIPAddressBasePadded = str_pad($userIPAddressBase, $tokenPadLength, "0", STR_PAD_LEFT);

            //sha512 of salt just for fun
            $sha512OfSalt = hash('sha512', $tokenSalt);

            //sha1 of HTTP USER AGENT, this way we catch is the cookie is moved to a different browser type/version
            $sha1UserAgent = sha1($_SERVER['HTTP_USER_AGENT']);

            //hash of the data that we are going to validate.
            $data = $timestampBasePadded . $randomBasePadded . $userIDBasePadded . $sha512OfSalt . $userIPAddressBasePadded . $sha1UserAgent;
            $sha512 = hash('sha512', $data . $tokenSalt);

            //lets start with the hash of the userAgent
            $firstPart = substr($sha1UserAgent,0,$tokenPadLength);
            $secondPart = substr($sha1UserAgent,$tokenPadLength,strlen($sha1UserAgent));
            $token = $firstPart . $sha512OfSalt . $secondPart;

            //now lets add in the timestamp
            $firstPart = substr($token,0,$tokenPadLength);
            $secondPart = substr($token,$tokenPadLength,strlen($token));
            $token = $firstPart . $timestampBasePadded . $secondPart;

            //now lets add in the random number
            $firstPart = substr($token,0,($tokenPadLength + $baseConvertTimestampValue));
            $secondPart = substr($token,($tokenPadLength + $baseConvertTimestampValue),strlen($token));
            $token = $firstPart . $randomBasePadded . $secondPart;

            //now lets add in the userID
            $firstPart = substr($token,0,($tokenPadLength + $baseConvertRandomValue));
            $secondPart = substr($token,($tokenPadLength + $baseConvertRandomValue),strlen($token));
            $token = $firstPart . $userIDBasePadded . $secondPart;

            //now lets add in the Users IP address
            $firstPart = substr($token,0,($tokenPadLength + $baseConvertUserIDValue));
            $secondPart = substr($token,($tokenPadLength + $baseConvertUserIDValue),strlen($token));
            $token = $firstPart . $userIPAddressBasePadded . $secondPart;

            //now lets add in the hash of it all
            $firstPart = substr($token,0,($tokenPadLength + $baseConvertIPAddressValue));
            $secondPart = substr($token,($tokenPadLength + $baseConvertIPAddressValue),strlen($token));
            $token = $firstPart . $sha512 . $secondPart;

            return $token;
        }
        else{
            return false;
        }
    }


    function on_invalid_authentication(){
        return !$this->isAuthenticated();
    }

    function on_valid_authentication(){
        return $this->isAuthenticated();
    }


    public function isAuthenticated() {
        require (APPPATH.'/config/token_authentication.php');

        $authenticationSuccess = false;

        if(isset($_COOKIE[$cookieName])){
            $token = $_COOKIE[$cookieName];
            if(UserAuthentication::verifyAuthenticationToken($token)){
                $authenticationSuccess = true;
            }
        }
        if(!$authenticationSuccess){
            return false;
        }
        else{
            return true;
        }
    }



    public function verifyAuthenticationToken($token,$returnArray = false){

        require (APPPATH.'/config/token_authentication.php');


        //lets make sure the token is a string
        $token = filter_var($token,FILTER_SANITIZE_STRING);

        //now start by striping out the sha512 hash
        $firstPart = substr($token,0,($tokenPadLength + $baseConvertIPAddressValue));
        $sha512 = substr($token,($tokenPadLength + $baseConvertIPAddressValue),128);
        $thirdPart = substr($token,($tokenPadLength + $baseConvertIPAddressValue + 128),(strlen($token)-($tokenPadLength + $baseConvertIPAddressValue + 128)));
        $token = $firstPart . $thirdPart;
        //echo "sha512[" . strlen($sha512) . "] = " . $sha512 . "<br>";

        //now lets strip out the users IP address
        $firstPart = substr($token,0,($tokenPadLength + $baseConvertUserIDValue));
        $userIPAddressBasePadded = substr($token,($tokenPadLength + $baseConvertUserIDValue),$tokenPadLength);
        $thirdPart = substr($token,($tokenPadLength + $baseConvertUserIDValue + $tokenPadLength),(strlen($token)-($tokenPadLength + $baseConvertUserIDValue + $tokenPadLength)));
        $token = $firstPart . $thirdPart;
        //echo "IP Address[" . strlen($userIPAddressBasePadded) . "] = " . $userIPAddressBasePadded . " = " . long2ip(base_convert($userIPAddressBasePadded,$baseConvertIPAddressValue, 10)) .  "<br>";

        //now lets strip out the users userID
        $firstPart = substr($token,0,($tokenPadLength + $baseConvertRandomValue));
        $userIDBasePadded = substr($token,($tokenPadLength + $baseConvertRandomValue),$tokenPadLength);
        $thirdPart = substr($token,($tokenPadLength + $baseConvertRandomValue + $tokenPadLength),(strlen($token)-($tokenPadLength + $baseConvertRandomValue + $tokenPadLength)));
        $token = $firstPart . $thirdPart;
        //echo "UserID[" . strlen($userIDBasePadded) . "] = " . $userIDBasePadded . " = " . base_convert($userIDBasePadded,$baseConvertUserIDValue, 10) .  "<br>";

        //now lets strip out the random number
        $firstPart = substr($token,0,($tokenPadLength + $baseConvertTimestampValue));
        $randomBasePadded = substr($token,($tokenPadLength + $baseConvertTimestampValue),$tokenPadLength);
        $thirdPart = substr($token,($tokenPadLength + $baseConvertTimestampValue + $tokenPadLength),(strlen($token)-($tokenPadLength + $baseConvertTimestampValue + $tokenPadLength)));
        $token = $firstPart . $thirdPart;
        //echo "Random Number[" . strlen($randomBasePadded) . "] = " . $randomBasePadded . " = " . base_convert($randomBasePadded,$baseConvertRandomValue, 10) .  "<br>";

        //now lets strip out the timestamp
        $firstPart = substr($token,0,$tokenPadLength);
        $timestampBasePadded = substr($token,$tokenPadLength,$tokenPadLength);
        $thirdPart = substr($token,$tokenPadLength +$tokenPadLength,(strlen($token) - ($tokenPadLength + $tokenPadLength)));
        $token = $firstPart . $thirdPart;
        //echo "Timestamp[" . strlen($timestampBasePadded) . "] = " . $timestampBasePadded . " = " . base_convert($timestampBasePadded,$baseConvertTimestampValue, 10) .   "<br>";

        //now lets strip out the SHA512 of the salt
        $firstPart = substr($token,0,$tokenPadLength);
        $sha512OfSalt = substr($token,$tokenPadLength,128);
        $thirdPart = substr($token,$tokenPadLength + 128,(strlen($token) - ($tokenPadLength + 128)));
        $token = $firstPart . $thirdPart;
        //echo "sha512OfSalt[" . strlen($sha512OfSalt) . "] = " . $sha512OfSalt .  "<br>";

        //now we know that what is left is the hash of the userAgent
        $sha1UserAgent = $token;
        //echo "sha1OfUserAgent[" . strlen($sha1UserAgent) . "] = " . $sha1UserAgent .  "<br>";

        //now lets build the data string to hash and make sure the token was not tampered with
        $data = $timestampBasePadded . $randomBasePadded . $userIDBasePadded . $sha512OfSalt . $userIPAddressBasePadded . $sha1UserAgent;

        //set some variables so you don't get too confused
        $cookieHash = $sha512;
        $hashOfData = hash('sha512', $data . $tokenSalt);

        $cookieIP = base_convert($userIPAddressBasePadded,$baseConvertIPAddressValue, 10);
        $currentIP = ip2long($_SERVER['REMOTE_ADDR']);

        $cookieTimestamp = base_convert($timestampBasePadded,$baseConvertTimestampValue, 10);
        $currentTimestamp = time();

        $cookieUserAgentHash = $sha1UserAgent;
        $userAgentHash = sha1($_SERVER['HTTP_USER_AGENT']);

        //not used
        //$cookieRandomNumber = base_convert($randomBasePadded,$baseConvertRandomValue, 10);

        $cookieUserID = base_convert($userIDBasePadded,$baseConvertUserIDValue, 10);

        //let make sure nothing fishy went on and make sure the hashes match first
        if($cookieHash != $hashOfData){
            return false;
        }
        elseif($cookieUserAgentHash != $userAgentHash) {
            return false;
        }
        else if($cookieIP != $currentIP) {
            return false;
        }
        else if($cookieTimestamp < $currentTimestamp - $maximumIdleTime){
            return false;
        }
        else {

            return true;
        }
    }

}
?>
