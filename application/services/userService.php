<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'libraries/doctrine.php';
require_once APPPATH . 'services/userGroupService.php';

class UserService
{

    static function getAllUsers()
    {
        try {
            $doctrine = new Doctrine();
            $em = $doctrine->em;
            $users = $em->getRepository("\Entity\User")->findAll();
            $result = array();
            foreach ($users as $user) {
                array_push($result, DTOMapper::mapUser($user));
            }
            return $result;
        } catch(Exception $ex){
            throw new Exception( $ex ->getMessage());
        }

    }

    static function getUserById($id)
    {
        try {
            $doctrine = new Doctrine();
            $em = $doctrine->em;
            $user = $em->find("\Entity\User", $id);
            $em->close();
            if ($user !=null){
                return DTOMapper::mapUser($user);
            }else{
                return false;
            }
        }catch (Exception $ex){
            throw new Exception( $ex ->getMessage());
        }

    }

    static function updateUser($id, $put_data)
    {
        try {
            $doctrine = new Doctrine();
            $em = $doctrine->em;
            $user = $em->find("\Entity\User", $id);
            if (!is_null($put_data['groupId'])){
                $group = $em->find("\Entity\UserGroup", $put_data['groupId']);
                if ($group != null){
                    $user->setGroup($group);
                }
                else{
                    return false;
                }
            }
            if (!is_null($put_data['Username'])){
                $user->setUserName($put_data['Username']);
            }
            if (!is_null($put_data['FirstName'])){
                $user->setFirstName($put_data['FirstName']);
            }
            if (!is_null($put_data['LastName'])){
                $user->setLastName($put_data['LastName']);
            }
            if (!is_null($put_data['Email'])){
                $user->setEmail($put_data['Email']);
            }
            $user->setLastUpdated();
            if (array_key_exists('Password',$put_data)){
                $user->setPasswordHashed(UserService::hashPassword($put_data['Password']));
            }
            $em->flush();
            return true;
        }
        catch (exception $ex) {
            throw new Exception( $ex ->getMessage());
        }

    }

    static function addUser($post_data)
    {
        try {
            $new_user = new \Entity\User();
            $doctrine = new Doctrine();
            $em = $doctrine->em;
            $new_user->setUserName($post_data['Username']);
            $new_user->setFirstName($post_data['FirstName']);
            $new_user->setLastName($post_data['LastName']);
            $new_user->setEmail($post_data['Email']);
            if (!is_null($post_data['Photos'])){
                $new_user->setProfilePhotos($post_data['Photos']);
            }
            $new_user->setRegistered();
            $new_user->setLastUpdated();
            $new_user->setIsActivated(true);
            $new_user->setPasswordHashed(UserService::hashPassword($post_data['Password']));
            $group = UserGroupService::getUserGroupById($post_data['groupId']);
            if ($group != null){
                $new_user->setGroup($group);
            }
            else{
                return false;
            }
            $em->merge($new_user);
            $em->flush();
            return true;
        }
        catch (exception $ex) {
            throw new Exception( $ex ->getMessage());
        }
    }

    static function deleteUser($id){
        try {
            $doctrine = new Doctrine();
            $em = $doctrine -> em;
            $user = $em->find("\Entity\User", $id);
            $em ->remove($user);
            $em ->flush();
            return true;
        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }
    }

    static function findUser($username){
        $doctrine = new Doctrine();
        $em = $doctrine -> em;
        $user = $em ->getRepository("\Entity\User") ->findOneBy(array('Username' => $username));
        return $user;
    }

    static function hashPassword($password)
    {
        $encrypt = new CI_Encrypt();
        return $encrypt->encode($password);
    }
    
}
