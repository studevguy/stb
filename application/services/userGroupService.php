<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'/libraries/doctrine.php';
  
class UserGroupService {

    static function getAllUserGroups(){
        try {
            $doctrine = new Doctrine();
            $em = $doctrine->em;
            $groups = $em->getRepository("\Entity\UserGroup") -> findAll();
            $result = array();
            foreach($groups as $group){
                array_push($result, DTOMapper::mapUserGroup($group));
            }
            return $result;
        }catch (Exception $ex){
            throw new Exception( $ex ->getMessage());
        }

    }

	static function getUserGroupById ($id){
        try {
            $doctrine = new Doctrine();
            $em = $doctrine->em;
            $group = $em->find("\Entity\UserGroup", $id);
            return $group;
        }catch (Exception $ex){
            throw new Exception( $ex ->getMessage());
        }

	}
}