<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . 'libraries/doctrine.php';
require_once APPPATH . 'models/Entity/Sales.php';
require_once APPPATH . 'models/Entity/SalesStats.php';
require_once APPPATH . 'view_models/NumberOfVisitsRevisitsPerExhibition.php';
include APPPATH . 'libraries/geoiploc.php';

class SalesService
{
    private $title;
    private $em;

    function __construct(){
        $this ->title = 'any';
        $doctrine = new Doctrine();
        $this  ->em = $doctrine ->em;
    }

    function getNumberOfVisitsRevisitsPerExhibition($startDay, $endDate){
        $numberOfVisitBySalesIdListResult = Array();
        try {
            $visitsDQL = 'SELECT sales.Title AS title, COUNT(sales.ID) AS visits FROM \Entity\SalesStats sales_stats LEFT JOIN sales_stats.sales sales GROUP BY sales.ID';
            $revisitsDQL =  'SELECT sales.Title AS title, sales_stats.original_ip AS original_ip, COUNT(sales_stats.original_ip)-1 AS revisits_per_ip FROM \Entity\SalesStats sales_stats LEFT JOIN sales_stats.sales sales GROUP BY sales.ID, sales_stats.original_ip';
            if(($startDay != 0) && ($endDate != 0)){
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            $visitsDQL = 'SELECT sales.Title AS title, COUNT(sales.ID) AS visits FROM \Entity\SalesStats sales_stats LEFT JOIN sales_stats.sales sales  WHERE (sales_stats.date > ?1) AND (sales_stats.date < ?2) GROUP BY sales.ID';
                $revisitsDQL = 'SELECT sales.Title AS title, sales_stats.original_ip AS original_ip, COUNT(sales_stats.original_ip)-1 AS revisits_per_ip FROM \Entity\SalesStats sales_stats LEFT JOIN sales_stats.sales sales WHERE (sales_stats.date > ?1) AND (sales_stats.date < ?2) GROUP BY sales.ID, sales_stats.original_ip';
            }
            $visitsQuery = $this ->em -> createQuery($visitsDQL);
            $revisitsQuery = $this ->em -> createQuery($revisitsDQL);
            if(($startDay != 0) && ($endDate != 0)){
                $visitsQuery ->setParameter(1, $startDay);
                $visitsQuery ->setParameter(2, $endDate);
                $revisitsQuery ->setParameter(1, $startDay);
                $revisitsQuery ->setParameter(2, $endDate);
            }
            $numberOfVisitBySalesIdList = $visitsQuery ->getResult();
            $numberOfRevisitBySalesIdList = $revisitsQuery ->getResult();
        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

        foreach ($numberOfVisitBySalesIdList as $item){
            $this ->title =  $item['title'];
            $new_item = new stdClass();
            $new_item ->Name = $this -> title;
            $new_item ->Visits = $item['visits'];
            $new_item ->Revisits = 0;

            //calculaet revisits
            foreach ($numberOfRevisitBySalesIdList as $item){
                if($item['title']==$this ->title){
                    $new_item ->Revisits = $new_item ->Revisits + $item['revisits_per_ip'];
                }
            }

            array_push($numberOfVisitBySalesIdListResult, $new_item);
        }
        return $numberOfVisitBySalesIdListResult;
    }

    function getTimeSpentForPage(){

        try {

            $timeSpentQuery = $this ->em ->createQuery('select Sum(sales_stats.time_spent_in_gallery_view) as time_spent_in_gallery_view , sum(sales_stats.time_spent_in_item_view) as time_spent_in_item_view, sum(sales_stats.time_spent_in_home_view) as time_spent_in_home_view from \Entity\SalesStats sales_stats');
        $timeSpentData = $timeSpentQuery ->getArrayResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }


        $data1 = new stdClass();
		$data1->PageName = "ItemView";
		$data1->TimeSpent = $this ->convertTimeSpentInViewToPercent($timeSpentData[0],$timeSpentData[0]['time_spent_in_item_view']);

        $data2 = new stdClass();
        $data2->PageName = "GalleryView";
        $data2->TimeSpent = $this ->convertTimeSpentInViewToPercent($timeSpentData[0],$timeSpentData[0]['time_spent_in_gallery_view']);

        $data3 = new stdClass();
        $data3->PageName = "HomeView";
        $data3->TimeSpent = 100 -$data2->TimeSpent-$data1->TimeSpent ;

		$result = array($data1, $data2, $data3);
        //$result = json_encode($mockup_data);
        return $result;
    }

    function getTimeSpentForPlatform (){
            $result = array();

        try {

            $timeSpentQuery = $this ->em -> createQuery('SELECT sales_stats.platform, SUM(sales_stats.duration) as duration FROM \Entity\SalesStats sales_stats GROUP BY sales_stats.platform');
            $timeSpentData = $timeSpentQuery ->getResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

            //convert duration to minute unit
        for ($i = 0; $i<count($timeSpentData); $i++){
            $timeSpentData[$i]['duration'] = round (round($timeSpentData[$i]['duration']/60));
        }

        foreach ($timeSpentData as $item){
            switch ($item['platform']){
                case 0:
                   $this -> addNewTimeSpentRecordToArray($result, 'IOS6', $item['duration']);
                    break;
                case 1:
                    $this -> addNewTimeSpentRecordToArray($result, 'IOS7', $item['duration']);
                    break;
                case 2:
                    $this -> addNewTimeSpentRecordToArray($result, 'IOS8', $item['duration']);
                    break;
                case 100:
                    $this -> addNewTimeSpentRecordToArray($result, 'Android 2.x', $item['duration']);
                    break;
                case 101:
                    $this -> addNewTimeSpentRecordToArray($result, 'Android 3.x', $item['duration']);
                    break;
                case 102:
                    $this -> addNewTimeSpentRecordToArray($result, 'Android 4.x', $item['duration']);
                    break;
                case 200:
                    $this -> addNewTimeSpentRecordToArray($result, 'Browser(Windows)', $item['duration']);
                    break;
                case 201:
                    $this -> addNewTimeSpentRecordToArray($result, 'Browser(Mac)', $item['duration']);
                    break;
                case 202:
                    $this -> addNewTimeSpentRecordToArray($result, 'Browser(Linux)', $item['duration']);
                    break;
            }
        }
        return $result;
    }

    function getPopularSearchCriteriaData (){

        try {

            $searchCriteriaQuery = $this ->em ->createQuery('SELECT SUM(sales_stats.search_by_lot) AS by_lot, SUM (sales_stats.search_by_title) AS by_title FROM \Entity\SalesStats sales_stats');
            $searchCriteriaData = $searchCriteriaQuery ->getResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

        //calculate percent
        $sum = array_sum($searchCriteriaData[0]);
        $searchCriteriaData[0]['by_lot'] = round($searchCriteriaData[0]['by_lot']/$sum*100,2);
        $searchCriteriaData[0]['by_title']  = 100 - $searchCriteriaData[0]['by_lot'];

        $obj1 = new stdClass();
        $obj1->Keyword = "By lot";
        $obj1->Hit =  $searchCriteriaData[0]['by_lot'] ;
        $obj2 = new stdClass();
        $obj2->Keyword = "By title";
        $obj2->Hit =  $searchCriteriaData[0]['by_title'] ;

        $result = array($obj1, $obj2);

        return $result;
    }


   function getVisitPerExhibitionByCountry($original){

       try {

           //list after transform ip address field to national code
           $viewOfExhibitionData = array();

           $originalFilter = $original;
           $result = array();
           $exhibitionTitleList =array();

           $rawViewOfExhibitionDataQuery = $this -> em ->createQuery('SELECT sales.Title, sales_stats.original_ip, COUNT (sales_stats.original_ip) AS visits FROM \Entity\SalesStats sales_stats LEFT JOIN sales_stats.sales sales GROUP BY sales.Title, sales_stats.original_ip');
           $rawViewOfExhibitionData = $rawViewOfExhibitionDataQuery -> getResult();

           $exhibitionTitleQuery = $this ->em ->createQuery('SELECT DISTINCT sales.Title FROM \Entity\SalesStats sales_stats LEFT JOIN sales_stats.sales sales');
           $tempExhibitionTitleList = $exhibitionTitleQuery -> getArrayResult();

       }
       catch (exception $ex){
           throw new Exception( $ex ->getMessage());
       }


       foreach ($tempExhibitionTitleList as $item){
           array_push($exhibitionTitleList, $item['Title']);
       }

       for ($i=0; $i<count($rawViewOfExhibitionData); $i++){
           if(getCountryFromIP($rawViewOfExhibitionData[$i]['original_ip'])==$originalFilter){
               $rawViewOfExhibitionData[$i]['original_ip'] = getCountryFromIP($rawViewOfExhibitionData[$i]['original_ip']);
               array_push($viewOfExhibitionData, $rawViewOfExhibitionData[$i]);
           }
       }

       //if filter has no result
       if (count($viewOfExhibitionData) < 1 ){
           return false;
       }

       //sum all visits that belong to same title
       foreach($exhibitionTitleList as $title){

           $visitsSum =0;

           foreach($viewOfExhibitionData as $item){
              if ($item['Title']==$title){
                  $visitsSum += $item['visits'];
              }
          }
            if ($visitsSum>0){
                $obj = new stdClass();
                $obj ->Name = $title;
                $obj ->Views = $visitsSum;

                array_push($result, $obj);
            }
       }

       return $result;
   }

    function getExhibitionList(){

        $result = array();

        try {

            $exhibitionQuery = $this ->em ->createQuery('SELECT sales.ID,sales.Title FROM \Entity\Sales sales ORDER BY sales.Title');
            $exhibitionList = $exhibitionQuery ->getResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

        foreach ($exhibitionList as $item){
            $obj = new stdClass();
            $obj ->Id = $item['ID'];
            $obj ->Name = $item['Title'];
            array_push($result, $obj);
        }

        return $result;
    }

    function getClickOnByExhibition ($sales_id){
        try {

            $clickOnQuery = $this -> em ->createQuery("SELECT SUM(item_stats.visualised_in_home) AS visualised_in_home, SUM(item_stats.register_to_bid_used) AS register_to_bid_used, SUM(item_stats.contact_specialist_used) AS contact_specialist_used FROM \Entity\ItemStats item_stats LEFT JOIN item_stats.salesStats sales_stats LEFT JOIN sales_stats.sales sales WHERE sales.ID =?1");
            $clickOnQuery->setParameter(1, $sales_id);
            $clickOnData = $clickOnQuery ->getResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

        if (count($clickOnData) < 0){
            return false;
        }

        if ( !$clickOnData[0]['visualised_in_home']
            && !$clickOnData[0]['contact_specialist_used']
            && !$clickOnData[0]['register_to_bid_used'] ){

            return false;
        }

        $obj1 = new stdClass();
        $obj1->Name = "Visualised in Home";
        $obj1->Views = $clickOnData[0]['visualised_in_home'] != null ? $clickOnData[0]['visualised_in_home'] : 0;
        $obj2 = new stdClass();
        $obj2->Name = "Contact Specialist";
        $obj2->Views = $clickOnData[0]['contact_specialist_used'] != null ? $clickOnData[0]['contact_specialist_used'] : 0;
        $obj3 = new stdClass();
        $obj3->Name = "Register to Bid";
        $obj3->Views = $clickOnData[0]['register_to_bid_used'] != null ? $clickOnData[0]['register_to_bid_used'] : 0;
        $result = array($obj1, $obj2, $obj3);

        return $result;

    }

    function getPopularLotsBySales ($exhibitionId){

        $result = array();

        try {

            $viewsByLotNumberQuery = $this -> em ->createQuery('SELECT item_stats.lot_number, SUM (item_stats.view_count) AS views  FROM \Entity\ItemStats item_stats LEFT JOIN item_stats.salesStats sales_stats LEFT JOIN sales_stats.sales sales WHERE sales.ID =?1 AND item_stats.duration > 5 GROUP BY item_stats.lot_number');
            $viewsByLotNumberQuery = $viewsByLotNumberQuery -> setParameter(1, $exhibitionId);
            $viewsByLotNumberData = $viewsByLotNumberQuery -> getResult();

            $multipleViewsCountByLotNumberQuery = $this -> em ->createQuery('SELECT item_stats.lot_number, COUNT (item_stats.view_count) AS multiple_views  FROM \Entity\ItemStats item_stats LEFT JOIN item_stats.salesStats sales_stats LEFT JOIN sales_stats.sales sales WHERE sales.ID =?1 AND item_stats.duration > 5 AND item_stats.view_count > 1 GROUP BY item_stats.lot_number');
            $multipleViewsCountByLotNumberQuery = $multipleViewsCountByLotNumberQuery ->setParameter(1, $exhibitionId);
            $multipleViewsCountByLotNumberData = $multipleViewsCountByLotNumberQuery -> getResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

        if(count($viewsByLotNumberData) < 0){
            return false;
        }

        foreach($viewsByLotNumberData as $item ){
            $obj = new stdClass();
            $obj ->Lot = $item['lot_number'];
            $obj ->Views = $item['views'];
            $obj ->MultipleViews = 0;
            //get multiplesvies by current lot_number
            $temp = array_filter($multipleViewsCountByLotNumberData, function($item) use ($obj){
               return $item['lot_number'] == $obj ->Lot;
            });
            if ($temp){
                $obj ->MultipleViews = $temp[0]['multiple_views'];
            }
            array_push($result, $obj);
        }

        return $result;

    }

    function getTotalDownloads (){

        try {

            $totalDownloadsQuery = $this ->em ->createQuery('SELECT COUNT (sales_stats.id) as total_downloads FROM \Entity\SalesStats sales_stats');
            $totalDownloads = $totalDownloadsQuery ->getResult();

        }
        catch (exception $ex){
            throw new Exception( $ex ->getMessage());
        }

        $result =new stdClass();
        $result->Total = $totalDownloads[0]['total_downloads'];

        return $result;
    }

    private function convertTimeSpentInViewToPercent ($arrayToSum, $itemValue){
        $sum = array_sum($arrayToSum);
        return round($itemValue/$sum*100,2);
    }

    private function addNewTimeSpentRecordToArray(& $array, $platform, $duration){
        $obj = new stdClass();
        $obj->OperatingSystem = $platform;
        $obj->TimeSpent = $duration;
        array_push($array, $obj);
    }

}