<?php

namespace DoctrineProxies\__CG__\Entity;

/**
 * THIS CLASS WAS GENERATED BY THE DOCTRINE ORM. DO NOT EDIT THIS FILE.
 */
class ItemStats extends \Entity\ItemStats implements \Doctrine\ORM\Proxy\Proxy
{
    private $_entityPersister;
    private $_identifier;
    public $__isInitialized__ = false;
    public function __construct($entityPersister, $identifier)
    {
        $this->_entityPersister = $entityPersister;
        $this->_identifier = $identifier;
    }
    /** @private */
    public function __load()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;

            if (method_exists($this, "__wakeup")) {
                // call this after __isInitialized__to avoid infinite recursion
                // but before loading to emulate what ClassMetadata::newInstance()
                // provides.
                $this->__wakeup();
            }

            if ($this->_entityPersister->load($this->_identifier, $this) === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            unset($this->_entityPersister, $this->_identifier);
        }
    }

    /** @private */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    
    public function getArtist()
    {
        $this->__load();
        return parent::getArtist();
    }

    public function setArtist($artist)
    {
        $this->__load();
        return parent::setArtist($artist);
    }

    public function getContactSpecialistUsed()
    {
        $this->__load();
        return parent::getContactSpecialistUsed();
    }

    public function setContactSpecialistUsed($contact_specialist_used)
    {
        $this->__load();
        return parent::setContactSpecialistUsed($contact_specialist_used);
    }

    public function getDate()
    {
        $this->__load();
        return parent::getDate();
    }

    public function setDate(\DateTime $date)
    {
        $this->__load();
        return parent::setDate($date);
    }

    public function getDuration()
    {
        $this->__load();
        return parent::getDuration();
    }

    public function setDuration($duration)
    {
        $this->__load();
        return parent::setDuration($duration);
    }

    public function getHiresViewCount()
    {
        $this->__load();
        return parent::getHiresViewCount();
    }

    public function setHiresViewCount($hires_view_count)
    {
        $this->__load();
        return parent::setHiresViewCount($hires_view_count);
    }

    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int) $this->_identifier["id"];
        }
        $this->__load();
        return parent::getId();
    }

    public function setId($id)
    {
        $this->__load();
        return parent::setId($id);
    }

    public function getLotNumber()
    {
        $this->__load();
        return parent::getLotNumber();
    }

    public function setLotNumber($lot_number)
    {
        $this->__load();
        return parent::setLotNumber($lot_number);
    }

    public function getObjectNumber()
    {
        $this->__load();
        return parent::getObjectNumber();
    }

    public function setObjectNumber($object_number)
    {
        $this->__load();
        return parent::setObjectNumber($object_number);
    }

    public function getRegisterToBidUsed()
    {
        $this->__load();
        return parent::getRegisterToBidUsed();
    }

    public function setRegisterToBidUsed($register_to_bid_used)
    {
        $this->__load();
        return parent::setRegisterToBidUsed($register_to_bid_used);
    }

    public function getRequestedFrom3d()
    {
        $this->__load();
        return parent::getRequestedFrom3d();
    }

    public function setRequestedFrom3d($requested_from_3d)
    {
        $this->__load();
        return parent::setRequestedFrom3d($requested_from_3d);
    }

    public function getRequestedFromItemDetail()
    {
        $this->__load();
        return parent::getRequestedFromItemDetail();
    }

    public function setRequestedFromItemDetail($requested_from_item_detail)
    {
        $this->__load();
        return parent::setRequestedFromItemDetail($requested_from_item_detail);
    }

    public function getRequestedFromSearch()
    {
        $this->__load();
        return parent::getRequestedFromSearch();
    }

    public function setRequestedFromSearch($requested_from_search)
    {
        $this->__load();
        return parent::setRequestedFromSearch($requested_from_search);
    }

    public function getSaleStatId()
    {
        $this->__load();
        return parent::getSaleStatId();
    }

    public function setSaleStatId($sale_stat_id)
    {
        $this->__load();
        return parent::setSaleStatId($sale_stat_id);
    }

    public function getTitle()
    {
        $this->__load();
        return parent::getTitle();
    }

    public function setTitle($title)
    {
        $this->__load();
        return parent::setTitle($title);
    }

    public function getViewCount()
    {
        $this->__load();
        return parent::getViewCount();
    }

    public function setViewCount($view_count)
    {
        $this->__load();
        return parent::setViewCount($view_count);
    }

    public function getVisualisedInHome()
    {
        $this->__load();
        return parent::getVisualisedInHome();
    }

    public function setVisualisedInHome($visualised_in_home)
    {
        $this->__load();
        return parent::setVisualisedInHome($visualised_in_home);
    }


    public function __sleep()
    {
        return array('__isInitialized__', 'id', 'sale_stat_id', 'date', 'duration', 'object_number', 'lot_number', 'artist', 'title', 'view_count', 'requested_from_3d', 'requested_from_search', 'requested_from_item_detail', 'visualised_in_home', 'hires_view_count', 'register_to_bid_used', 'contact_specialist_used');
    }

    public function __clone()
    {
        if (!$this->__isInitialized__ && $this->_entityPersister) {
            $this->__isInitialized__ = true;
            $class = $this->_entityPersister->getClassMetadata();
            $original = $this->_entityPersister->load($this->_identifier);
            if ($original === null) {
                throw new \Doctrine\ORM\EntityNotFoundException();
            }
            foreach ($class->reflFields as $field => $reflProperty) {
                $reflProperty->setValue($this, $reflProperty->getValue($original));
            }
            unset($this->_entityPersister, $this->_identifier);
        }
        
    }
}