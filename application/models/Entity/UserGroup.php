<?php
namespace Entity;

/**
 * User Group Model
 *
 * @Entity
 * @Table(name="user_group")
 * @author  Joseph Wynn <joseph@wildlyinaccurate.com>
 */
class UserGroup
{

/**
	 * Initialize any collection properties as ArrayCollections
	 *
	 * http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/association-mapping.html#initializing-collections
	 *
	 */


	/**
	 * @Id
	 * @Column(type="integer", nullable=false)
	 * @GeneratedValue(strategy="AUTO")
	 */
	protected $Id;

	 /**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->Id;
	}

	/**
	 * @Column(type="string", length=32, unique=true, nullable=false)
	 */
	protected $Name;

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Group
	 */
	public function setName($name)
	{
		$this->Name = $name;
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->Name;
	}

	/**
	 * @OneToMany(targetEntity="User", mappedBy="Group")
	 */
	protected $Users;

	/**
	 * Add users
	 *
	 * @param Entity\User $users
	 * @return Group
	 */
	public function addUser(\Entity\User $users)
	{
		$this->Users[] = $users;
		return $this;
	}

	/**
	 * Get users
	 *
	 * @return Doctrine\Common\Collections\Collection
	 */
		public function getUsers()
	{
		return $this->Users;
	}
	
}
