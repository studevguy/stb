<?php
/**
 * Created by IntelliJ IDEA.
 * User: tiger
 * Date: 06/10/2014
 * Time: 11:31
 */

namespace Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="sales")
 */
class Sales {

    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $ID;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
    protected $Company;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
    protected $UID;

    /**
     * @Column(type="string", length=255, nullable=false)
     */
    protected $Title;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    protected $Location;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    protected $Building;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $BuildingDownload;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $DateTime;

    /**
     * @Column(type="date", nullable=true)
     */
    protected $Expiry;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    protected $Username;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $Hash;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $LastUpdated;

    /**
     * @Column(type="string", length=1024, nullable=true)
     */
    protected $Description;

    /**
     * @Column(type="string", length=512, nullable=true)
     */
    protected $Website;

    /**
     * @Column(type="string", length=512, nullable=true)
     */
    protected $Specialist;

    /**
     * @Column(type="string", length=60, nullable=true)
     */
    protected $ExhibitionType;

    /**
     * @Column(type="string", length=120, nullable=true)
     */
    protected $InitialGallery;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $DataVersion;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $inactive;

    /**
     * @OneToMany(targetEntity="SalesStats", mappedBy="sales")
     */
    protected $sales_stats;

    function  _construct(){
        $this ->sales_stats = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getBuilding()
    {
        return $this->Building;
    }

    /**
     * @param mixed $Building
     */
    public function setBuilding($Building)
    {
        $this->Building = $Building;
    }

    /**
     * @return mixed
     */
    public function getBuildingDownload()
    {
        return $this->BuildingDownload;
    }

    /**
     * @param mixed $BuildingDownload
     */
    public function setBuildingDownload($BuildingDownload)
    {
        $this->BuildingDownload = $BuildingDownload;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->Company;
    }

    /**
     * @param mixed $Company
     */
    public function setCompany($Company)
    {
        $this->Company = $Company;
    }

    /**
     * @return mixed
     */
    public function getDataVersion()
    {
        return $this->DataVersion;
    }

    /**
     * @param mixed $DataVersion
     */
    public function setDataVersion($DataVersion)
    {
        $this->DataVersion = $DataVersion;
    }

    /**
     * @return mixed
     */
    public function getDateTime()
    {
        return $this->DateTime;
    }

    /**
     * @param mixed $DateTime
     */
    public function setDateTime(\DateTime $DateTime)
    {
        $this->DateTime = $DateTime;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->Description;
    }

    /**
     * @param mixed $Description
     */
    public function setDescription($Description)
    {
        $this->Description = $Description;
    }

    /**
     * @return mixed
     */
    public function getExhibitionType()
    {
        return $this->ExhibitionType;
    }

    /**
     * @param mixed $ExhibitionType
     */
    public function setExhibitionType($ExhibitionType)
    {
        $this->ExhibitionType = $ExhibitionType;
    }

    /**
     * @return mixed
     */
    public function getExpiry()
    {
        return $this->Expiry;
    }

    /**
     * @param mixed $Expiry
     */
    public function setExpiry(\DateTime $Expiry)
    {
        $this->Expiry = $Expiry;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->Hash;
    }

    /**
     * @param mixed $Hash
     */
    public function setHash($Hash)
    {
        $this->Hash = $Hash;
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getInitialGallery()
    {
        return $this->InitialGallery;
    }

    /**
     * @param mixed $InitialGallery
     */
    public function setInitialGallery($InitialGallery)
    {
        $this->InitialGallery = $InitialGallery;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->LastUpdated;
    }

    /**
     * @param mixed $LastUpdated
     */
    public function setLastUpdated(\DateTime $LastUpdated)
    {
        $this->LastUpdated = $LastUpdated;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->Location;
    }

    /**
     * @param mixed $Location
     */
    public function setLocation($Location)
    {
        $this->Location = $Location;
    }

    /**
     * @return mixed
     */
    public function getSpecialist()
    {
        return $this->Specialist;
    }

    /**
     * @param mixed $Specialist
     */
    public function setSpecialist($Specialist)
    {
        $this->Specialist = $Specialist;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->Title;
    }

    /**
     * @param mixed $Title
     */
    public function setTitle($Title)
    {
        $this->Title = $Title;
    }

    /**
     * @return mixed
     */
    public function getUID()
    {
        return $this->UID;
    }

    /**
     * @param mixed $UID
     */
    public function setUID($UID)
    {
        $this->UID = $UID;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * @param mixed $Username
     */
    public function setUsername($Username)
    {
        $this->Username = $Username;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->Website;
    }

    /**
     * @param mixed $Website
     */
    public function setWebsite($Website)
    {
        $this->Website = $Website;
    }

    /**
     * @return mixed
     */
    public function getInactive()
    {
        return $this->inactive;
    }

    /**
     * @param mixed $inactive
     */
    public function setInactive($inactive)
    {
        $this->inactive = $inactive;
    }

    /**
     * @return mixed
     */
    public function getSalesStats()
    {
        return $this->sales_stats;
    }

    /**
     * @param mixed $sales_stats
     */
    public function addSalesStats(SalesStats $sales_stats)
    {
        $this->sales_stats[] = $sales_stats;
    }
} 
