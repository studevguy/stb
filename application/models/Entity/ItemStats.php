<?php
namespace Entity;
/**
 * @Entity
 * @Table(name="item_stats")
 */
class ItemStats {
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="SalesStats",cascade={"merge"})
     * @JoinColumn(name="sales_stats_id", referencedColumnName="id")
     */
    protected $salesStats;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $date;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $duration;

    /**
     * @Column(type="string", length=64, nullable=true)
     */
    protected $object_number;

    /**
     * @Column(type="string", length=64, nullable=true)
     */
    protected $lot_number;

    /**
     * @Column(type="string", length=128, nullable=true)
     */
    protected $artist;

    /**
     * @Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $view_count;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $requested_from_3d;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $requested_from_search;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $requested_from_item_detail;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $visualised_in_home;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $hires_view_count;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $register_to_bid_used;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $contact_specialist_used;

    /**
     * @return mixed
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param mixed $artist
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    }

    /**
     * @return mixed
     */
    public function getContactSpecialistUsed()
    {
        return $this->contact_specialist_used;
    }

    /**
     * @param mixed $contact_specialist_used
     */
    public function setContactSpecialistUsed($contact_specialist_used)
    {
        $this->contact_specialist_used = $contact_specialist_used;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getHiresViewCount()
    {
        return $this->hires_view_count;
    }

    /**
     * @param mixed $hires_view_count
     */
    public function setHiresViewCount($hires_view_count)
    {
        $this->hires_view_count = $hires_view_count;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLotNumber()
    {
        return $this->lot_number;
    }

    /**
     * @param mixed $lot_number
     */
    public function setLotNumber($lot_number)
    {
        $this->lot_number = $lot_number;
    }

    /**
     * @return mixed
     */
    public function getObjectNumber()
    {
        return $this->object_number;
    }

    /**
     * @param mixed $object_number
     */
    public function setObjectNumber($object_number)
    {
        $this->object_number = $object_number;
    }

    /**
     * @return mixed
     */
    public function getRegisterToBidUsed()
    {
        return $this->register_to_bid_used;
    }

    /**
     * @param mixed $register_to_bid_used
     */
    public function setRegisterToBidUsed($register_to_bid_used)
    {
        $this->register_to_bid_used = $register_to_bid_used;
    }

    /**
     * @return mixed
     */
    public function getRequestedFrom3d()
    {
        return $this->requested_from_3d;
    }

    /**
     * @param mixed $requested_from_3d
     */
    public function setRequestedFrom3d($requested_from_3d)
    {
        $this->requested_from_3d = $requested_from_3d;
    }

    /**
     * @return mixed
     */
    public function getRequestedFromItemDetail()
    {
        return $this->requested_from_item_detail;
    }

    /**
     * @param mixed $requested_from_item_detail
     */
    public function setRequestedFromItemDetail($requested_from_item_detail)
    {
        $this->requested_from_item_detail = $requested_from_item_detail;
    }

    /**
     * @return mixed
     */
    public function getRequestedFromSearch()
    {
        return $this->requested_from_search;
    }

    /**
     * @param mixed $requested_from_search
     */
    public function setRequestedFromSearch($requested_from_search)
    {
        $this->requested_from_search = $requested_from_search;
    }

    /**
     * @return mixed
     */
    public function getSalesStats()
    {
        return $this->salesStats;
    }

    /**
     * @param mixed $sale_stat_id
     */
    public function setSalesStats(SalesStats $salesStats)
    {
        $this->salesStats = $salesStats;
        if(!$salesStats -> getItemStats() -> contain($this)){
            $salesStats -> addItemStats($this);
        }
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getViewCount()
    {
        return $this->view_count;
    }

    /**
     * @param mixed $view_count
     */
    public function setViewCount($view_count)
    {
        $this->view_count = $view_count;
    }

    /**
     * @return mixed
     */
    public function getVisualisedInHome()
    {
        return $this->visualised_in_home;
    }

    /**
     * @param mixed $visualised_in_home
     */
    public function setVisualisedInHome($visualised_in_home)
    {
        $this->visualised_in_home = $visualised_in_home;
    }

} 
