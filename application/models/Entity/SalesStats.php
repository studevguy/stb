<?php


namespace Entity;

/**
 * @Entity
 * @Table(name="sales_stats")
 */
class SalesStats {
    /**
     * @Id
     * @Column(type="integer", nullable=false)
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ManyToOne(targetEntity="Sales",cascade={"merge"})
     * @JoinColumn(name="sales_id", referencedColumnName="ID")
     */
    protected  $sales;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $date;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $duration;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $items_viewed;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $items_viewed_in_home;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $register_to_bid_used;

    /**
     * @Column(type="integer", nullable=true)
     */
    protected $contact_specialist_used;

    /**
     * @OneToMany(targetEntity="ItemStats", mappedBy="salesStats")
     */
    protected $itemStats;

    /**
     * @return mixed
     */
    public function getContactSpecialistUsed()
    {
        return $this->contact_specialist_used;
    }

    /**
     * @param mixed $contact_specialist_used
     */
    public function setContactSpecialistUsed($contact_specialist_used)
    {
        $this->contact_specialist_used = $contact_specialist_used;
    }

    /**
     * @Column(type="string", length=64, nullable=true)
     */
    protected $original_ip;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $platform;

    /**
     * @Column(type="decimal", precision=10, scale=0)
     */
    protected $time_spent_in_gallery_view;

    /**
     * @Column(type="decimal", precision=10, scale=0)
     */
    protected $time_spent_in_item_view;

    /**
     * @Column(type="decimal", precision=10, scale=0)
     */
    protected $time_spent_in_home_view;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $search_by_lot;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $search_by_title;


    /**
     * @Column(type="integer", nullable=true)
     */
    protected $department;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param mixed $sale_id
     */
    public function setSale(Sales $sales)
    {
        $this->sales = $sales;
        if(!$sales -> getSalesStats() -> contain($this)){
            $sales -> addSalesStats($this);
        }
    }


    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getItemsViewed()
    {
        return $this->items_viewed;
    }

    /**
     * @param mixed $items_viewed
     */
    public function setItemsViewed($items_viewed)
    {
        $this->items_viewed = $items_viewed;
    }

    /**
     * @return mixed
     */
    public function getItemsViewedInHome()
    {
        return $this->items_viewed_in_home;
    }

    /**
     * @param mixed $items_viewed_in_home
     */
    public function setItemsViewedInHome($items_viewed_in_home)
    {
        $this->items_viewed_in_home = $items_viewed_in_home;
    }

    /**
     * @return mixed
     */
    public function getOriginalIp()
    {
        return $this->original_ip;
    }

    /**
     * @param mixed $original_ip
     */
    public function setOriginalIp($original_ip)
    {
        $this->original_ip = $original_ip;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    /**
     * @return mixed
     */
    public function getRegisterToBidUsed()
    {
        return $this->register_to_bid_used;
    }

    /**
     * @param mixed $register_to_bid_used
     */
    public function setRegisterToBidUsed($register_to_bid_used)
    {
        $this->register_to_bid_used = $register_to_bid_used;
    }

    /**
     * @return mixed
     */
    public function getSearchByLot()
    {
        return $this->search_by_lot;
    }

    /**
     * @param mixed $search_by_lot
     */
    public function setSearchByLot($search_by_lot)
    {
        $this->search_by_lot = $search_by_lot;
    }

    /**
     * @return mixed
     */
    public function getSearchByTitle()
    {
        return $this->search_by_title;
    }

    /**
     * @param mixed $search_by_title
     */
    public function setSearchByTitle($search_by_title)
    {
        $this->search_by_title = $search_by_title;
    }

    /**
     * @return mixed
     */
    public function getTimeSpentInGalleryView()
    {
        return $this->time_spent_in_gallery_view;
    }

    /**
     * @param mixed $time_spent_in_gallery_view
     */
    public function setTimeSpentInGalleryView($time_spent_in_gallery_view)
    {
        $this->time_spent_in_gallery_view = $time_spent_in_gallery_view;
    }

    /**
     * @return mixed
     */
    public function getTimeSpentInHomeView()
    {
        return $this->time_spent_in_home_view;
    }

    /**
     * @param mixed $time_spent_in_home_view
     */
    public function setTimeSpentInHomeView($time_spent_in_home_view)
    {
        $this->time_spent_in_home_view = $time_spent_in_home_view;
    }

    /**
     * @return mixed
     */
    public function getTimeSpentInItemView()
    {
        return $this->time_spent_in_item_view;
    }

    /**
     * @param mixed $time_spent_in_item_view
     */
    public function setTimeSpentInItemView($time_spent_in_item_view)
    {
        $this->time_spent_in_item_view = $time_spent_in_item_view;
    }

    /**
     * @return mixed
     */
    public function getItemStats()
    {
        return $this->itemStats;
    }

    /**
     * @param mixed $sales_stats
     */
    public function addItemStats(ItemStats $itemStats)
    {
        $this->itemStats[] = $itemStats;
    }

} 
