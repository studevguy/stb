<?php
namespace Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * User Model
 *
 * @Entity
 * @Table(name="user")
 * @author  Joseph Wynn <joseph@wildlyinaccurate.com>
 */
class User
{
	/**
	 * @Id
	 * @Column(type="integer", nullable=false)
	 * @GeneratedValue(strategy="AUTO")
	 */
	protected $Id;


	public function getId()
	{
		return $this->Id;
	}

    /**
     * @Column(type="string", length=32, unique=true, nullable=false)
     */
    protected $Username;

    public function setUserName($username)
    {
        $this->Username = $username;
        return $this;
    }

    public function getUserName()
    {
        return $this->Username;

    }

	/**
	 * @Column(type="string", length=255, nullable=false)
	 */
	protected $FirstName;
	
	public function setFirstName($FirstName)
	{
		$this->FirstName = $FirstName;
		return $this;
	}

	public function getFirstName()
	{
		return $this->FirstName;
		
	}
	
	/**
	 * @Column(type="string", length=255, nullable=false)
	 */
	protected $LastName;
	
	public function setLastName($LastName)
	{
		$this->LastName = $LastName;
		return $this;
	}

	public function getLastName()
	{
		return $this->LastName;
	}
	
	/**
	 * @Column(type="string", length=255, unique=true, nullable=false)
	 */
	protected $Email;
	
	public function setEmail($Email)
	{
		$this->Email= $Email;
		return $this;
	}
	
	public function getEmail()
	{
		return $this->Email;
	}




	/**
	*@column(type="string", length=255, nullable=true)
	*/
	protected $ProfilePhotos;

	public function setProfilePhotos($ProfilePhotos)
	{
	$this->ProfilePhotos = $ProfilePhotos;
	return $this;
	}

	public function	getProfilePhotos()
	{
		return $this->ProfilePhotos;
	}

	/**
	*@column(type="datetime", nullable=true)
	*/
	protected $Registered;

	public function setRegistered()
	{
		$this->Registered = new \DateTime("now");
		return $this;
	}

	public function	getRegistered()
	{
		return $this->Registered;
	}

	/**
	*@column(type="datetime", nullable=true)
	*/
	protected $LastUpdated;

	public function setLastUpdated()
	{
		$this->LastUpdated = new \DateTime("now");
		return $this;
	}

	public function	getLastUpdated()
	{
		return $this->LastUpdated;
	}

	/**
	*@column(type="datetime", nullable=true)
	*/
	protected $LastLoggedOn;

	public function setLastLoggedOn($LastLoggedOn)
	{
		$this->LastLoggedOn = $LastLoggedOn;
		return $this;
	}

	public function	getLastLoggedOn()
	{
		return $this->LastLoggedOn;
	}

	/**
	*@column(type="boolean")
	*/
	protected $IsActivated;

	public function setIsActivated($IsActivated)
	{
		$this->IsActivated = $IsActivated;
		return $this;
	}

	public function	getIsActivated()
	{
		return $this->IsActivated;
	}

	/**
	 * @Column(type="string", length=255, nullable=false)
	 */
	protected $PasswordHashed;
	
	public function setPasswordHashed($password)
	{
		$this->PasswordHashed= $password;
	}
	
	public function getPasswordHashed()
	{
		return $this->PasswordHashed;
	}

	/**
	 * Encrypt a Password
	 *
	 * @param	string	$password
	 * @return	string
	 */

	
	/**
	 * @ManyToOne(targetEntity="UserGroup",cascade={"merge"})
	 * @JoinColumn(name="GroupId", referencedColumnName="Id")
	 */
	protected $Group;

	/**
	 * Assign the user to a group
	 *
	 * @param	Entity\UserGroup	$group
	 * @return	void
	 */
	public function setGroup(\Entity\UserGroup $group)
	{
		$this->Group = $group;

		// The association must be defined in both directions
		if ( ! $group->getUsers()->contains($this))
		{
			$group->addUser($this);
		}
	}

	/**
	 * Get group
	 *
	 * @return Entity\UserGroup
	 */
	public function getGroup()
	{
		return $this->Group;

	}

}
