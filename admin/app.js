﻿define([
    'text',
    'routeResolver',
    'angular-route',
    'resource',
    'angular-ui',
    'autoAppTemplateCache',
    'blockUI',
    'ngTable'
], function (config) {
    'use strict';

    var app = angular.module('autoApp',
    [
        'routeResolver',
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'autoAppTemplateCache',
        'blockUI',
        'ngTable',
    ]);

    app.register = app;
    app.settings = config;

    var rootUrlForViews = debugMode ? "{0}/".format(baseUrl) : "";

    app.rootUrlForViews = rootUrlForViews;
    app.config([
        '$routeProvider',
        'routeResolverSvcProvider',
        '$controllerProvider',
        '$compileProvider',
        '$filterProvider',
        '$provide',
        'blockUIConfigProvider',
        function ($routeProvider,
            routeResolverSvc,
            $controllerProvider,
            $compileProvider,
            $filterProvider,
            $provide,
            blockUIConfigProvider) {

            routeResolverSvc.routeConfig.setViewBaseDirectory(app.rootUrlForViews);
            app.register = {
                controller: $controllerProvider.register,
                directive: $compileProvider.directive,
                filter: $filterProvider.register,
                factory: $provide.factory,
                service: $provide.service
            };
            blockUIConfigProvider.autoBlock(false);
            blockUIConfigProvider.templateUrl('blockUITemplate.html');
        }
    ]);
    return app;
});