﻿define([
    'app',
    'config',
    'common/userService',
], function (app, config) {
    'use strict';
    app.register.controller('editUserGroupCtrl', [
        '$scope', '$http', '$modal',
        function ($scope, $http, $modal) {
            $scope.title = "Edit User Group";
        }
    ]);
});
