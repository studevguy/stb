﻿define([
    'app',
    'config',
    'common/userService',
    'common/eztek.angular.dataTables'
], function (app,config) {
    'use strict';
    app.register.controller('userListCtrl', ['$scope',
        'userSvc',
        'ngTableParams',
        '$filter',
        '$location',
        '$route',
        'blockUI',
        function ($scope,
                  userSvc,
                  ngTableParams,
                  $filter,
                  $location,
                  $route,
                  blockUI
            ) {

        var userListTableBlock = blockUI.instances.get('userListTable');
        userListTableBlock.start();
        userSvc.getUsers().success(function(data){
            $scope.users = data;
            $scope.tableParams = new ngTableParams({
                page: 1,
                count: 10
            },{
                counts: [],
                total: $scope.users.length,
                getData: function($defer, params) {
                    var orderedData = params.sorting()?$filter('orderBy')($scope.users, params.orderBy()):$scope.users;
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
            userListTableBlock.stop();
        }).error(function(e, status){
            userListTableBlock.stop();
            toastr.error("can not load the user list");
            if (status ==405){
                window.location.href="./login";
            }
        });

        $scope.deleteUser = function (userId) {
            userListTableBlock.start();
            userSvc.deleteUser(userId)
                .success(function (data) {
                    toastr.info("User has been deleted");
                    userListTableBlock.stop();
                    $route.reload();
                })
                .error(function (error, status) {
                    toastr.error("Delete user fail");
                    userListTableBlock.stop();
                    if (status ==405){
                        window.location.href="./login";
                    }
                });
        }
        $scope.$on('$viewContentLoaded', function(){
            var body = angular.element('body');
            body.addClass("loading-completed")
        });
    }]);
});
