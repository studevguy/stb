﻿define([
    'app',
    'config',
    'common/userService',
    'common/eztek.angular.dataTables'
], function (app,config) {
    'use strict';
    app.register.controller('userGroupListCtrl', [
        '$scope', '$http', '$modal',
        function ($scope, $http, $modal) {
            $scope.dataTableConfig = {
                ajaxUrl: config.USER_GROUP_DATATABLES_API_URL,
                columns: [
                    {
                        "data": "Id",
                        "title": "Id",
                        "searchable": false,
                        "sortable": false,
                        "width": 100
                    },
                    {
                        "data": "Name",
                        "title": "Name",
                        "searchPlaceHolder": "Search Group",
                    },
                    {
                        "data": "Users",
                        "title": "User",
                        "searchable": false,
                        "width": 180
                    },
                    {
                        "data": "LastUpdated",
                        "title": "LastUpdated",
                        "searchable": false,
                        "width": 180
                    },
                    {
                        "data": "LastUpdatedBy",
                        "title": "LastUpdatedBy",
                        "searchable": false,
                        "width": 180
                    }
                ],
                columnDefs: [
                    {
                        "targets": 0,
                        "render": function (data, type, row) {
                            if (!row.IsEditable)
                                return "";

                            return '<button function-edit class="btn btn-primary btn-xs">' +
                                '<i class="glyphicon glyphicon-pencil"></i>' +
                                '</button>&nbsp;' +
                                '<button function-delete class="btn btn-danger btn-xs">' +
                                '<i class="glyphicon glyphicon-remove"></i>' +
                                '</button>';
                        }
                    },
                    {
                        "targets": 3,
                        "render": function (data, type, row) {
                            var date = moment(data);
                            var format = date.format("ddd MMM-DD-YYYY, hh:mm:ss A Z");
                            return format + "&nbsp;(<em>{0}</em>)".format(date.fromNow());
                        }
                    }
                ],
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $(nRow).on("click", "[function-edit]", function (e) {
                        e.preventDefault();
                        $scope.updateUserGroup(aData);
                    });

                    $(nRow).on("click", "[function-delete]", function (e) {
                        e.preventDefault();
                        $scope.deleteUserGroup(aData);
                    });
                    return nRow;
                },
                drawTrigger: {}
            };

            $scope.createNewUserGroup = function () {
                require(['groupManagement/UserGroupDialogController'], function (userGroupDialogController) {
                    $modal.open({
                        templateUrl: config.userGroupDialogEditTmpl,
                        controller: 'UserGroupDialogController',
                        resolve: {
                            viewModel: createUserGroupViewModel
                        }
                    });
                });
            };

            $scope.updateUserGroup = function (userGroup) {
                $http({
                    url: config.USER_GROUP_API_URL.format(userGroup.Id),
                    method: "GET",
                }).success(function (userGroupData) {
                    require(['groupManagement/UserGroupDialogController'], function (userGroupDialogController) {
                        $modal.open({
                            templateUrl: config.userGroupDialogEditTmpl,
                            controller: 'UserGroupDialogController',
                            resolve: {
                                viewModel: function () {
                                    return updateUserGroupViewModel(userGroupData);
                                }
                            }
                        });
                    });
                }).error(function (data) {
                    toastr.error(data.responseText);
                });
            };

            $scope.deleteUserGroup = function (userGroup) {
                bootbox.confirm(i18n.t("UserManagement.Message.PromptDeleteGroup").format(userGroup.Name), function (confirmed) {
                    if (confirmed) {
                        $http({
                            url: config.USER_GROUP_API_URL.format(userGroup.Id),
                            method: "DELETE"
                        }).success(function (data) {
                            toastr.success(data.message);
                            $scope.dataTableConfig.drawTrigger = new Date();
                        }).error(function (data) {
                            toastr.error(data.errorMessage);
                        });
                    }
                });
            }

            function createUserGroupViewModel() {
                return {
                    userGroup: {
                        Name: "",
                        IsReadOnly: false,
                    },
                    title: i18n.t("UserManagement.Labels.CreateUserGroup"),
                    okButtonText: i18n.t("UserManagement.Buttons.CreateUserGroupBtn"),
                    okCallback: createUserGroup
                };
            }


            function createUserGroup($modalInstance, returnData) {
                $http({
                    url: config.CREATE_USER_GROUP_API_URL,
                    method: "POST",
                    data: returnData
                }).success(function () {
                    toastr.success("User group created");
                    $scope.dataTableConfig.drawTrigger = new Date();
                    $modalInstance.close();
                }).error(function (data) {
                    toastr.error(data.message);
                });

            }

            function updateUserGroupViewModel(userGroupData) {
                return {
                    userGroup: userGroupData,
                    title: i18n.t("UserManagement.Labels.UpdateUserGroup"),
                    okButtonText: i18n.t("UserManagement.Buttons.EditUserGroupSubmitBtn"),
                    okCallback: updateUserGroup
                };
            }

            function updateUserGroup($modalInstance, userGroupData) {
                $http({
                    url: USER_GROUP_API_URL.format(userGroupData.Id),
                    method: "PUT",
                    data: userGroupData
                }).success(function (data) {
                    toastr.success("User group saved");
                    $scope.dataTableConfig.drawTrigger = new Date();
                    $modalInstance.close();
                }).error(function (data) {
                    toastr.error(data.message);
                });
            }
        }
    ]);
});
