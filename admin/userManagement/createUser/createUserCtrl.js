﻿define([
    'app',
    'common/userService',
    'common/userGroupService',
], function (app) {
    'use strict';

    app.register.controller('createUserCtrl', [
        '$scope',
        'userSvc',
        '$location',
        'userGroupSvc',
        'blockUI',
        function (
            $scope,
            userSvc,
            $location,
            userGroupSvc,
            blockUI
            ) {
            $scope.title = "Create User";

            $scope.user = {};
            $scope.user.groupName = "Select User Group";

            $scope.dropdownStatus ={
                isOpen : false
            };

            $scope.selectUserGroup = function($event,id){
                $event.preventDefault();
                $scope.user.groupId = id;
                $scope.user.groupName = _.find($scope.userGroups, function(item){
                   return item.Id == id;
                }).Name;
                $scope.dropdownStatus.isOpen = false;
            };

            userGroupSvc.getAllUserGroups().success(function(data){
                $scope.userGroups = data;
            });

            $scope.cancelCreate = function () {
                history.go(-1);
            };

            $scope.createUser = function () {
                var createUserFormBlock = blockUI.instances.get('createUserForm');
                createUserFormBlock.start();
                userSvc.createUser($scope.user)
                    .success(function (data) {
                        toastr.success("Create successfully");
                        createUserFormBlock.stop();
                        $location.path("/userlist");
                    })
                    .error(function (error, status) {
                        if (status == 400){
                            toastr.error(error);
                        }else{
                            toastr.error("Create user fail");
                        }
                        createUserFormBlock.stop();
                        if (status ==405){
                            window.location.href="./login";
                        }
                    });
            };
        }
    ]);
});
