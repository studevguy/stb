﻿define([
    'app',
    'config',
    'common/userService',
], function (app,config) {
    'use strict';
    app.register.controller('createUserGroupCtrl', [
        '$scope', '$http', '$modal',
        function ($scope, $http, $modal) {
            $scope.title = "Create User Group";
        }
    ]);
});
