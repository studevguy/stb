﻿define([
    'app',
    'common/userService',
    'common/userGroupService',
], function (app) {
    'use strict';

    app.register.controller('editUserCtrl', [
        '$scope',
        'userSvc',
        '$location',
        '$routeParams',
        'userGroupSvc',
        'blockUI',
        function (
            $scope,
            userSvc,
            $location,
            $routeParams,
            userGroupSvc,
            blokcUI
            ) {
            $scope.title = "Edit User";

            var userId = $routeParams.id;
            var editUserFormBlock = blokcUI.instances.get('editUserForm');

            editUserFormBlock.start();

            userSvc.getUserById(userId).success(function(data){
                $scope.user = data;
                $scope.user.groupId = data.Group.Id;
                $scope.user.groupName = data.Group.Name;
                editUserFormBlock.stop();
            }).error(function(e, status){
                editUserFormBlock.stop();
                if (status == 405){
                    window.location.href="./login";
                }
            });

            userGroupSvc.getAllUserGroups().success(function(data){
                $scope.userGroups = data;
            });

            $scope.dropdownStatus ={
                isOpen : false
            };

            $scope.selectUserGroup = function($event,id){
                $event.preventDefault();
                $scope.user.groupId = id;
                $scope.user.groupName = _.find($scope.userGroups, function(item){
                    return item.Id ==id;
                }).Name;
                $scope.dropdownStatus.isOpen = false;
            };

            $scope.cancelCreate = function () {
                history.go(-1);
            };

            $scope.updateUser = function () {
                editUserFormBlock.start();
                userSvc.updateUser(userId,$scope.user)
                    .success(function (data) {
                        toastr.success("update successfully");
                        editUserFormBlock.stop();
                        $location.path("/userlist");
                    })
                    .error(function (error, status) {
                        if(status == 400){
                            toastr.error(error);
                        }else{
                            toastr.error("update user fail");
                        }
                        editUserFormBlock.stop();
                        if (status ==405){
                            window.location.href="./login";
                        }
                    });
            };
        }
    ]);
});
