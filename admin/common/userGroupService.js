﻿define([
    'app',
    'config'
] , function(app, config) {
    'use strict';

    app.register.factory('userGroupSvc' , [
        '$http' ,
        function($http) {
            
            return {
                getAllUserGroups: getAllUserGroups,
                getGroupById: getGroupById,
                createGroup: createGroup,
                updateGroup: updateGroup,
                deleteGroup: deleteGroup
            };

            function getAllUserGroups(){
                return $http({
                    method: "get",
                    url: config.group.USER_GROUPS_API_URL
                })
            }

            function getGroupById(groupId) {

            }

            function createGroup(data) {

            }

            function updateGroup(userId, data) {

            }

            function deleteGroup(userId) {

            }
        }
    ]);
});