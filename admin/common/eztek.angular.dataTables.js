﻿define(['app'], function (app) {
    'use strict';
    app.register.directive('eztekDataTable', [function () {
        return {
            scope: {
                configuration: "="
            },
            link: function (scope, element) {
                var options = {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "sDom": "Rlfrtip",
                    "bFilter": true,
                    "ajax": {
                        "type": "GET",
                        "accepts": "application/json"
                    }
                };

                var dataTableConfig = scope.configuration;

                if (dataTableConfig.columns) {
                    options["columns"] = dataTableConfig.columns;
                    _.each(options.columns, function (column) {
                        console.log(column.title);
                        if (!column.title)
                            column.title = column.data;
                    });
                }

                if (dataTableConfig.columnDefs) {
                    options["columnDefs"] = dataTableConfig.columnDefs;
                }

                if (dataTableConfig.ajaxUrl) {
                    options["ajax"]["url"] = dataTableConfig.ajaxUrl;
                }
                if (dataTableConfig.fnRowCallback) {
                    options["fnRowCallback"] = dataTableConfig.fnRowCallback;
                }

                var dataTable = element.DataTable(options);

                var containSearchableColumns = _.filter(options.columns, function (column) {
                    return !(column.searchable != null && !column.searchable);
                });

                if (_.size(containSearchableColumns) > 0) {
                    $("thead", element).append("<tr></tr>");

                    _.each(options.columns, function (column) {
                        var html = "<th>";
                        if (column.searchable == null || column.searchable == true) {
                            html += "<input class='form-control dataTable-filter'" +
                                " placeholder='" + (column.searchPlaceHolder || ("Search by " + column.data)) + "'" +
                                " />";
                        }
                        html += "</th>";
                        $("thead > tr:last-child").append(html);
                    });
                }

                $(element).on('keyup change', 'input.dataTable-filter', function () {
                    var index = $(this).closest("th").index();

                    dataTable.column(index).search(this.value).draw();
                });

                scope.$watch('configuration.drawTrigger', function () {
                    dataTable.draw(false);
                });
            }
        };
    }]);
});
