﻿define([
    'app',
    'config'
] , function(app, config) {
    'use strict';

    app.register.factory('userSvc' , [
        '$http' ,
        function($http) {
            
            return {
                getUsers: getUsers,
                createUser: createUser,
                getUserById: getUserById,
                updateUser: updateUser,
                deleteUser: deleteUser
            };

            function getUsers(){
                return $http({
                    method: "get",
                    url: config.user.USERS_API_URL
                })
            }

            function getUserById(userId) {
                return $http({
                    method: "GET",
                    url: config.user.USER_BY_ID_API_URL.format(userId)
                });
            }

            function createUser(data) {
                return $http({
                    method: "post",
                    url: config.user.USER_API_URL,
                    data: data
                });
            }

            function updateUser(userId, data) {
                return $http({
                    method: "PUT",
                    url: config.user.USER_BY_ID_API_URL.format(userId),
                    data: data
                });
            }

            function deleteUser(userId) {
                return $http({
                    method: "DELETE",
                    url: config.user.USER_BY_ID_API_URL.format(userId)
                });
            }
        }
    ]);
});