﻿define('config', function () {
    var
        userGroupsApiUrl = "index.php/adminapi/usergroups",
        usersApiUrl = "index.php/adminapi/users",
        userApiUrl = "index.php/adminapi/users/user",
        userByIdApiUrl = "index.php/adminapi/users/user/{0}";

       

    return {
        group: {
            USER_GROUPS_API_URL: userGroupsApiUrl,
        },
        user: {
            USERS_API_URL: usersApiUrl,
            USER_API_URL: userApiUrl,
            USER_BY_ID_API_URL: userByIdApiUrl
        },
    };
});