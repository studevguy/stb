﻿define([
    'app',
    'text!userManagement/userList/userList.html',
    'text!userManagement/createUser/createUser.html',
    'text!userManagement/editUser/editUser.html',
    'text!userManagement/userGroupList/userGroupList.html',
    'text!userManagement/editUserGroup/editUserGroup.html',
    'text!userManagement/createUserGroup/createUserGroup.html'
], function (app) {
    'use strict';
    var args = arguments;
    app.config([
        '$routeProvider',
        'routeResolverSvcProvider',
        function ($routeProvider, routeResolverSvcProvider) {
            //Define routes - controllers will be loaded dynamically
            var route = routeResolverSvcProvider.route;
            $routeProvider
                .when('/', route.resolve('userManagement/userList/userList', args[1]))
                .when('/createuser', route.resolve('userManagement/createUser/createUser', args[2]))
                .when('/edituser/:id', route.resolve('userManagement/editUser/editUser', args[3]))
                .when('/usergrouplist', route.resolve('userManagement/userGroupList/userGroupList', args[4]))
                .when('/editusergroup', route.resolve('userManagement/editUserGroup/editUserGroup', args[5]))
                .when('/createusergroup', route.resolve('userManagement/createUserGroup/createUserGroup', args[6]))
                .otherwise({
                    redirectTo: '/'
                });
        }
    ]);
});
