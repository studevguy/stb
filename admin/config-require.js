﻿define(function () {
    return {
        // Here paths are set relative to `/source/Scripts` folder
        baseUrl: baseUrl,
        paths: {
            'routeResolver': 'common/routeResolverSvc',
            'jQuery': '../Scripts/jquery-1.9.1',
            'underscore': '../underscore',
            'resource': '../Scripts/angular-resource',
            'angular-route': '../Scripts/angular-route',
            'angular-ui': '../Scripts/ui-bootstrap-tpls-0.11.0',
            'angular-moment': '../Scripts/angular-moment',
            'moment': '../Scripts/moment',
            'ui-utils': '../Scripts/vendors/ui-utils',
            'text': '../Scripts/text',
            'angular-bootstrap-select': '../Scripts/angular-bootstrap-select',
            'jquery.scrollbar': 'http://gromo.github.io/jquery.scrollbar/jquery.scrollbar',
            'ng-google-chart': '../Scripts/ng-google-chart',
            'blockUI': '../Scripts/angular-block-ui.min',
            'ngTable': '../Scripts/ng-table',
        },

        shim: {
            'jQuery': {
                'exports': '$'
            },
            'angular': {
                'exports': 'angular'
            },
            'underscore': {
                'exports': '_'
            },
            'resource': {
                'deps': ['angular'],
                'exports': 'resource'
            },
            'angular-route': {
                'deps': ['angular']
            },
            'angular-moment': {
                'deps': ['angular']
            },
            'angular-ui': {
                'deps': ['angular']
            },
            'ui-utils': {
                'deps': ['angular']
            },
            'angular-bootstrap-select': {
                'deps': ['angular']
            },
            'ng-google-chart': {
                'deps': ['angular']
            },
            'blockUI': {
                'deps': ['angular']
            },
        }
    };
});