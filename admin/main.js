﻿define([
    'app' ,
    'route'
] , function() {
    'use strict';

    angular.bootstrap(document , ['autoApp']);

    $('.main-content').addClass('active');
});