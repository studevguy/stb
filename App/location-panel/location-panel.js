define([
    'app',
    'text!./location-panel.html',
    './locationPanelCtrl'
], function(
    app,
    locationPanelTemplate
){
	'use strict';
	app.register.directive('locationPanel',[function(){

	    return {
            restrict: 'EA',
            template: locationPanelTemplate,
            controller: 'locationPanelCtrl',
            controllerAs: 'lpCtrl'
        }
	}]);
});