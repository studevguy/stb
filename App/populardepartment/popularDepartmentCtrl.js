﻿define(['app',
'common/soTheBySvc'
], function (app) {
    'use strict';
    app.register.controller('popularDepartmentCtrl', [
        'soTheBySvc', 'blockUI','exportDataSvc',
        function (soTheBySvc, blockUI,exportDataSvc) {
            var self = this;
            var formPopularDepartment = blockUI.instances.get('myBlockPopularDepartment');
            var obj = '';
            formPopularDepartment.start();
            soTheBySvc.getPopularDepartment().then(function (data) {
                obj = data;
                exportDataSvc.registerDataToExport(data, 'Popular Department (number of hits)');

                self.labels = [];

                self.values = [];


                $.each(obj, function (key, value) {
                    self.labels.push(key);
                    self.values.push(value / 1000000);
                });

                function maxPopularDepartment() {
                    var maxX = 0;
                    for (var x = 0; x < self.values.length; x++)
                        if (maxX < self.values[x])
                            maxX = self.values[x];
                    return maxX;
                }

                self.maxPopularDepartment = maxPopularDepartment();

                self.divId = ['item-Impressionist', 'item-Contemporary', 'item-painting', 'item-century', 'item-prints', 'item-photography', 'item-art', 'item-britishpainting'];

                for (var i = 0; i < self.values.length; i++) {
                    canvasHBar(self.divId[i], self.values[i], self.maxPopularDepartment);
                }
                formPopularDepartment.stop();
            }, function (status) {
                formPopularDepartment.stop();
                toastr.error(status);
            });

            function canvasHBar(id, data, xmax) {
                new RGraph.HBar({
                    id: id,
                    data: [data],
                    options: {
                        background: {
                            grid: false
                        },
                        xmax: xmax,
                        scale: {
                            decimals: 1
                        },
                        colors: {
                            self: ['#15ced0'],
                            sequential: true
                        },
                        text: {
                            color: '#fff'
                        },
                        labels: {
                        },
                        noxaxis: true,
                        gutter: {
                            left: 0,
                            top: 0
                        },
                        xlabels: false
                    }
                }).grow();
            };
        }]);
});