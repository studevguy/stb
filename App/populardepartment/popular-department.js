﻿define(['app',
        'text!./populardepartment.html',
        './popularDepartmentCtrl'
], function (app, template) {
    'use strict';
    app.register.directive('popularDepartment', [function () {
        return {
            restrict: 'EA',
            template: template,
            controller: 'popularDepartmentCtrl',
            controllerAs: 'popularDepartmentCtrl'
        };

    }]);
});