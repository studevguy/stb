﻿define([
    'app',
    'text!./time-spent-gallery.html',
    'timeSpentGallery/timeSpentGalleryCtrl'
], function (app, timespenttemplate) {
    'use strict';
    app.register.directive('timeSpentGallery', [
        function () {
            return {
                restrict: 'EAC',
                scope: {},
                controller: 'timeSpentGalleryCtrl',
                controllerAs: 'timeSpentGalleryCtrl',
                template: timespenttemplate
            };
        }]);
});