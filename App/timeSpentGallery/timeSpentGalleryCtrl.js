﻿define([
        'app',
        'common/soTheBySvc'
], function (app) {
    'use strict';
    app.register.controller('timeSpentGalleryCtrl', ['soTheBySvc', 'blockUI','exportDataSvc',
        function (soTheBySvc, blockUI,exportDataSvc) {
            var self = this;
            var piechart = {};
            var colorArray = ['#036668', '#079E9F', '#15CEE0'];
            var formTimeSpentGallery = blockUI.instances.get('myBlockTimeSpentGallery');


            formTimeSpentGallery.start();
            soTheBySvc.getTimeSpentGallery().then(function (json) {
                var chartData = soTheBySvc.convertJsonToTimeSpentGallery(json);
                exportDataSvc.registerDataToExport(json, 'Time spent in the Gallery View vs Info Page');

                piechart.data = {
                    "cols": [
                        { label: "PageName", type: "string" },
                        { label: "TimeSpent", type: "number" }
                    ],
                    "rows": chartData
                };

                piechart.type = "PieChart";

                piechart.options = {
                    backgroundColor: {
                        stroke: '#fff',
                        strokeWidth: 0,
                        fill: '#000'
                    },
                    displayExactValues: true,
                    fontSize: 10,
                    width: 281,
                    height: 135,
                    is3D: false,
                    chartArea: {
                        left: 10,
                        top: 15,
                        bottom: 15,
                        right: 10,
                        height: "100%",
                        width: "100%"
                    },
                    tooltip: {
                        trigger: 'none'
                    },
                    colors: colorArray,
                    enableInteractivity: false,
                    pieSliceBorderColor: 'black',
                    pieSliceText: 'percentage',
                    pieSliceTextStyle: {
                        color: 'black'
                    },
                    legend: {
                        position: 'right',
                        textStyle: {
                            color: 'white'
                        }
                    },
                    reverseCategories: false,
                    pieResidueSliceColor: '#000'
                    //sliceVisibilityThreshold: 100/720,
                };

                self.piechart = piechart;
                var total = 0;

                formTimeSpentGallery.stop();
            }, function (status) {
                formTimeSpentGallery.stop();
                toastr.error(status);
            });
        }]);
});