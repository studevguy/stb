define([
    'app',
    'popular-lots-by-sale/popular-lots-by-sale',
    '../averagetimespent/average-time-spent',
    '../populardepartment/popular-department',
    'averagetimespent/average-time-spent',
    'viewsofexhibition/viewsofexhibition',
    'timeSpentGallery/time-spent-gallery',
    'popularSearchCriteria/popular-search-criteria',
    'clickexhibition/click-exhibition',
    '../numbervisitsexhibition/Number-visits-exhibition',
    'totalDownload/total-download',
    'common/ngJvectorMap',
    'templates/top-panel/top-panel',
    'location-panel/location-panel',
    'common/exportDataSvc',
    'date-filter/date-filter'
], function (app) {
    'use strict';
    app.register.controller('homeCtrl', [
        '$scope',
        '$rootScope',
        'exportDataSvc',
        'soTheBySvc',
        'dateFilterSvc',
        'ngDialog',
        function (
            $scope,
            $rootScope,
            exportDataSvc,
            soTheBySvc,
            dateFilterSvc,
            ngDialog
            ) {

            var filterDialog;
            $scope.filterDisplay = dateFilterSvc.filterDisplay;

            //check if it's the first page load
            if (soTheBySvc.countUrl()<1){
                soTheBySvc.setFilter(0,0);
            }
            $scope.getData = function(){
                return exportDataSvc.getData();
            };
            $scope.fileName = 'report.csv';
            $scope.$on('$viewContentLoaded', function(){
                var body = angular.element('body');
                body.addClass("loading-completed")
            });

            $scope.openDateFilterDialog = function(){

                filterDialog = ngDialog.open({
                    template: '<div date-filter></div>',
                    plain: true,
                    closeByDocument:false
                })
            }

        }
    ]);
});
