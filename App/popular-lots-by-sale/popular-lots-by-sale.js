﻿define(['app',
        'text!./popular-lots-by-sale.html',
        './popularLotsBySaleCtrl',
        'common/tinyscrollbar'
], function (app, popularLotsBySale) {
    'use strict';
    app.register.directive('popularLotsBySale', [function () {
        return {
            restrict: 'EA',
            priority: 10,
            template: popularLotsBySale,
            controller: 'popularLotsBySaleCtrl',
            controllerAs: 'popularLotsBySaleCtrl'
        };

    }]);
});