﻿define(['app',
        'common/soTheBySvc',
        'common/tinyscrollbarSvc',
        'common/onFinishRender'
], function (app) {
    'use strict';
    app.register.controller('popularLotsBySaleCtrl',

        ['$scope', 'soTheBySvc', 'tinyscrollbarSvc', 'blockUI','$filter',
            function ($scope, soTheBySvc, tinyscrollbarSvc, blockUI, $filter) {
                var self = this, exhibitionName;
                var formPopularLotsBySale = blockUI.instances.get('myBlockPopularLotsBySale');

                formPopularLotsBySale.start();
                self.tabs = [
                    { active: true },
                    { active: false }
                ];
                soTheBySvc.getExhibitions()
                .then(function (data) {
                    self.exhibitions = data;
                    self.exhibitionId = data[0].Id;
                    exhibitionName = data[0].Name;
                    getpopularlotsbysale();
                }, function (status) {
                    formPopularLotsBySale.stop();
                    toastr.error(status);
                });
                self.loadPopularLotsBySale = function () {
                    formPopularLotsBySale.start();
                    self.viewMostPopular = null;
                    self.viewLotOrders = null;
                    getSelectedExhibitionName();
                    getpopularlotsbysale();
                    
                };
                self.tnOptions = {
                    axis: "x",
                    thumbSize: 14,
                    trackSize: false
                };

                var getpopularlotsbysale = function () {
                    soTheBySvc.getPopularLotsBySale(self.exhibitionId).then(function (data) {
                        var popularLotsBySaleData = _.map(data, function(item){
                            item.Views =  parseInt(item.Views);
                            item.MultipleViews = parseInt(item.MultipleViews);
                            if (!isNaN(parseInt(item.Lot))){
                                item.Lot = parseInt(item.Lot);
                            }
                            return item;
                        });

                        self.viewMostPopular = $filter('orderBy')(popularLotsBySaleData,'-Views');
                        self.viewLotOrders = $filter('orderBy')(popularLotsBySaleData,'Lot');
                        formPopularLotsBySale.stop();
                    }, function (status) {
                        formPopularLotsBySale.stop();
                        toastr.error('There is no "Lots" data for ' + exhibitionName + ' exhibition');
                    });
//                    soTheBySvc.getLotOrders(self.exhibitionId).then(function (data) {
//                        self.viewLotOrders = data;
//                        formPopularLotsBySale.stop();
//                    }, function (status) {
//                        formPopularLotsBySale.stop();
//                        toastr.error(status);
//                    });
                };
                $scope.$on('mostpopularfirst', function () {
                    var $element = '#mostpopularfirst';
                    tinyscrollbarSvc.tinyscrollbar($element, self.tnOptions);
                    tinyscrollbarSvc.update($element);
                });
                $scope.$on('lotorder', function () {
                    var $element = '#lotorder';
                    tinyscrollbarSvc.tinyscrollbar($element, self.tnOptions);
                    tinyscrollbarSvc.update($element);
                });

                function getSelectedExhibitionName(){
                    exhibitionName = _.find(self.exhibitions, function(item){
                        return item.Id == self.exhibitionId;
                    }).Name;
                }
            }]);
});