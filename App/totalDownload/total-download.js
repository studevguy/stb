﻿define([
        'app',
        'text!./total-download.html',
        './totalDownloadCtrl'
], function (app, totaldownloadtemplate) {
    'use strict';
    app.register.directive('totalDownload', [function () {

        return {
            restrict: 'EA',
            template: totaldownloadtemplate,
            controller: 'totalDownloadCtrl',
            controllerAs: 'totalDownloadCtrl'
        };
    }]);
});