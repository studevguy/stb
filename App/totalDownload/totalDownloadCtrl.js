﻿define([
        'app',
        'common/soTheBySvc',
        'common/ngJvectorMap'
],
    function (app) {
        'use strict';
        app.register.controller('totalDownloadCtrl',
            ['soTheBySvc', 'blockUI',
            function (soTheBySvc, blockUI) {
                
                var self = this;
                var myBlockTotalDownload = blockUI.instances.get('myBlockTotalDownload');

                myBlockTotalDownload.start();
                soTheBySvc.getTotalDownloads().then(function (data) {
                    self.total = abbrNum(data.Total, 3);
                    myBlockTotalDownload.stop();
                }, function (status) {
                    myBlockTotalDownload.stop();
                    toastr.error(status);
                });

            }]);
    });