﻿define(['app'], function (app) {
    'use strict';
    'common/soTheBySvc',
    app.register.controller('clickExhibitionCtrl', ['$scope', 'soTheBySvc', 'blockUI','exportDataSvc',
        function ($scope, soTheBySvc, blockUI,exportDataSvc) {

            var self = this,
                selectedExhibitionName;
            var opts = {
                lines: 12, // The number of lines to draw
                angle: 0.25, // The length of each line
                lineWidth: 0.23, // The line thickness
                pointer: {
                    length: 0.65, // The radius of the inner circle
                    strokeWidth: 0.035, // The rotation offset
                    color: '#fff' // Fill color
                },
                limitMax: 'false',   // If true, the pointer will not go past the end of the gauge
                colorStart: '#15ced0',   // Colors
                colorStop: '#15ced0',    // just experiment with them
                strokeColor: '#5c5c5c'   // to see which ones work best for you
            };

            var animationSpeedValue = 30;

            var formClickOnExhibition = blockUI.instances.get('myBlockClickOnExhibition');
            formClickOnExhibition.start();

            soTheBySvc.getExhibitions().then(function (data) {
                self.exhibitions = data;
                self.exhibitionId = data[0].Id;
                selectedExhibitionName = data[0].Name;
                getClickExhibition();
                formClickOnExhibition.stop();
            }, function (status) {
                toastr.error(status);
                formClickOnExhibition.stop();
            });
            self.loadClickExhibition = function () {
                formClickOnExhibition.start();
                getSelectedExhitionName();
                getClickExhibition();
            };
            var getClickExhibition = function () {
                soTheBySvc.getClickExhibition(self.exhibitionId).then(function (data) {
                    self.clickExhibitionData = data;

                    exportDataSvc.registerDataToExport(data, 'Click on by Exhibition - '+selectedExhibitionName,'CE');

                    designCharts(self.clickExhibitionData);

                    formClickOnExhibition.stop();
                }, function (status) {
                    formClickOnExhibition.stop();

                    self.clickExhibitionData = _.map(self.clickExhibitionData, function(item){
                        item.Views = 0
                        return item;
                    } );

                    if (self.clickExhibitionData.length >0){
                        designCharts(self.clickExhibitionData);
                    }

                    toastr.error('There is no "click on exhibition" data for ' + selectedExhibitionName + ' exhibition');
                });
            };

            //for table name when exporting data
            function getSelectedExhitionName(){
                selectedExhibitionName = _.find(self.exhibitions, function(item){
                    return item.Id==self.exhibitionId;
                }).Name
            }

            function  designCharts(clickExhibitionData){
                self.totalNumberOfView = 0;
                for (var i = 0; i < clickExhibitionData.length; i++) {
                    self.totalNumberOfView += parseInt(clickExhibitionData[i].Views);
                    clickExhibitionData[i].abbrNumViews = abbrNum(clickExhibitionData[i].Views)
                }
                var maxValue = self.totalNumberOfView;
                self.totalNumberOfView = abbrNum(self.totalNumberOfView, 2);
                var realworldchartValue = clickExhibitionData[0];
                var contactchartValue = clickExhibitionData[1];
                var registerchartValue = clickExhibitionData[2];

                var realworldchartTarget = document.getElementById('realworldchart'); // your canvas element
                var realworldchartGauge = new Gauge(realworldchartTarget).setOptions(opts); // create sexy gauge!
                realworldchartGauge.maxValue = maxValue; // set max gauge value
                realworldchartGauge.animationSpeed = animationSpeedValue; // set animation speed (32 is default value)
                realworldchartGauge.set(realworldchartValue.Views); // set actual value

                var contactchartTarget = document.getElementById('contactchart'); // your canvas element
                var contactchartGauge = new Gauge(contactchartTarget).setOptions(opts); // create sexy gauge!
                contactchartGauge.maxValue = maxValue; // set max gauge value
                contactchartGauge.animationSpeed = animationSpeedValue; // set animation speed (32 is default value)
                contactchartGauge.set(contactchartValue.Views); // set actual value

                var registerchartTarget = document.getElementById('registerchart'); // your canvas element
                var registerchartGauge = new Gauge(registerchartTarget).setOptions(opts); // create sexy gauge!
                registerchartGauge.maxValue = maxValue; // set max gauge value
                registerchartGauge.animationSpeed = animationSpeedValue; // set animation speed (32 is default value)
                registerchartGauge.set(registerchartValue.Views); // set actual value
            }
        }]);
});