﻿define(['app',
        'text!./clickexhibition.html',
        './clickExhibitionCtrl'
], function (app, template) {
    'use strict';
    app.register.directive('clickExhibition', [function () {
        return {
            restrict: 'EA',
            template: template,
            controller: 'clickExhibitionCtrl',
            controllerAs: 'clickExhibitionCtrl'
        };

    }]);
});