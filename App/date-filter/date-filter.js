define([
    'app',
    'text!./date-filter.html',
    './dateFilterCtrl',
    './dateFilterSvc'
], function(
    app,
    tpl
){
	'use strict';
	app.register.directive('dateFilter',[function(){

        return {
            restrict: 'EA',
            template: tpl,
            replace:true,
            controller: 'dateFilterCtrl',
            controllerAs: 'dateFilterVm'
        }
	}]);
});
