define([
    'app',
], function(
    app
){
	'use strict';
	app.register.controller('dateFilterCtrl',[
        '$scope',
        'soTheBySvc',
        '$route',
        'dateFilterSvc',
        function(
            $scope,
            soTheBySvc,
            $route,
            dateFilterSvc
            ){

            var self = this;
            var BY_START_END_DATE = 1, BY_MONTH = 2, BY_QUARTER = 3, ALL_DATES_FILTER = 0;
            var filterMode = ALL_DATES_FILTER;

            self.isAllDateFilter = true;

            self.filter = {
                start:'',
                end:''
            };

            self.applyFilter = function(){
                switch (filterMode){
                    case BY_START_END_DATE:
                        self.filter.start = moment(self.startDate).format('YYYY-MM-DD');
                        self.filter.end = moment(self.endDate).format('YYYY-MM-DD');
                        soTheBySvc.setFilter(self.filter.start,self.filter.end);
                        $route.reload();
                        break;
                    case BY_MONTH:
                        var month = moment(self.month);
                        self.filter.start = month.startOf('month').format('YYYY-MM-DD');
                        self.filter.end = month.endOf('month').format('YYYY-MM-DD');
                        soTheBySvc.setFilter(self.filter.start,self.filter.end);
                        $route.reload();
                        break;
                    case BY_QUARTER:
                        var quarter = moment(self.yearOfQuarter).quarter(self.filterQuarter);
                        self.filter.start = quarter.startOf('quarter').format('YYYY-MM-DD');
                        self.filter.end = quarter.endOf('quarter').format('YYYY-MM-DD');
                        soTheBySvc.setFilter(self.filter.start,self.filter.end);
                        $route.reload();
                        break;
                    default:

                }
            };

            self.selectFilterMode = function(mode){
                filterMode = mode;
                self.isAllDateFilter = 0;
            };

            self.selectDefaultFilter = function(){
              filterMode = !self.isAllDateFilter ? ALL_DATES_FILTER : filterMode;
            }

	}]);
});
