﻿define(['app',
        'text!./number-visits-exhibition.html',
        './numberVisitsExhibitionCtrl'
], function (app, template) {
    'use strict';
    app.register.directive('numberVisitsExhibition', [function () {
        return {
            restrict: 'EA',
            priority:10,
            template: template,
            controller: 'numberVisitsExhibitionCtrl',
            controllerAs: 'numberVisitsExhibitionCtrl'
        };

    }]);
});
