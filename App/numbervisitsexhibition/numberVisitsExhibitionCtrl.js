﻿define(['app',
    'common/soTheBySvc',
        'common/tinyscrollbarSvc',
        'common/onFinishRender'
], function (app) {
    'use strict';
    app.register.controller('numberVisitsExhibitionCtrl',
        ['soTheBySvc', 'tinyscrollbarSvc', '$scope', 'blockUI','exportDataSvc',
        function (soTheBySvc, tinyscrollbarSvc, $scope, blockUI, exportDataSvc) {
            var self = this;
            var formNumberVistisExhibition = blockUI.instances.get('myBlockNumberVistisExhibition');
            formNumberVistisExhibition.start();
            soTheBySvc.getNumberVisitsExhibition().then(function (data) {
                self.numbeVisitsAndRevisitData = data;
                exportDataSvc.registerDataToExport(data, 'Number of Visits/Revisits per Exhibition');

                _.each(self.numbeVisitsAndRevisitData, function (item) {
                    item.Visits = abbrNum(item.Visits, 3);
                    item.Revisits = abbrNum(item.Revisits, 3);
                });
                formNumberVistisExhibition.stop();
            }, function (status) {
                toastr.error(status);
                formNumberVistisExhibition.stop();
            });
            self.tnOptions = {
                axis: "y",
                thumbSize: 6,
                trackSize: 174
            };
            $scope.$on('number-visits-revisits', function () {
                var $element = '#number-visits-revisits';
                tinyscrollbarSvc.tinyscrollbar($element, self.tnOptions);
            });
        }]);
});