﻿define([
        'routeResolver',
        'angular-route',
        'resource',
        'angular-ui',
        'angular-moment',
        'ui-utils',
        'text',
        'angular-bootstrap-select',
        'ng-google-chart',
        'autoAppTemplateCache',
        'blockUI',
        'ngCsv',
        'ngDialog',
    'ngDatePicker'
], function (config) {
    'use strict';
    angular.module('jm.i18next').config(['$i18nextProvider', function ($i18nextProvider) {
        'use strict';
        $i18nextProvider.options = {
            lng: 'en-US',
            useCookie: false,
            useLocalStorage: false,
            resGetPath: 'multilanguage/__lng__.json'
        };
    }]);

    var app = angular.module('autoApp',
    [
        'routeResolver',
        'ngRoute',
        'ngResource',
        'jm.i18next',
        'ui.bootstrap',
        'autoAppTemplateCache',
        'angularMoment',
        'ui.route',
        'angular-bootstrap-select',
        'angular-bootstrap-select.extra',
        'googlechart',
        'blockUI',
        'ngCsv',
        'ngDialog',
        'datePicker'
    ]);

    app.register = app;
    app.settings = config;

    var rootUrlForViews = debugMode ? "{0}/".format(baseUrl) : "";

    app.rootUrlForViews = rootUrlForViews;
    app.config([
        '$routeProvider',
        'routeResolverSvcProvider',
        '$controllerProvider',
        '$compileProvider',
        '$filterProvider',
        '$provide',
        'blockUIConfigProvider',
        function ($routeProvider,
            routeResolverSvc,
            $controllerProvider,
            $compileProvider,
            $filterProvider,
            $provide,
            blockUIConfigProvider) {

            routeResolverSvc.routeConfig.setViewBaseDirectory(app.rootUrlForViews);
            app.register = {
                controller: $controllerProvider.register,
                directive: $compileProvider.directive,
                filter: $filterProvider.register,
                factory: $provide.factory,
                service: $provide.service
            };
            blockUIConfigProvider.autoBlock(false);
            blockUIConfigProvider.templateUrl('blockUITemplate.html');
        }
    ]);
    return app;
});