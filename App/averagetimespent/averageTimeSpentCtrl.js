﻿define(['app',
    'common/soTheBySvc'
], function (app) {
    'use strict';
    app.register.controller('averageTimeSpentCtrl', [
        'soTheBySvc',
        'blockUI',
        'exportDataSvc',
        function (
            soTheBySvc,
            blockUI,
            exportDataSvc
            ) {
            var self = this;
            var formAverageTimeSpent = blockUI.instances.get('myBlockAverageTimeSpent');
            formAverageTimeSpent.start();

            soTheBySvc.getAverageTimeSpent().then(function (data) {
                self.averageTimeSpent = _.sortBy(data, function(item){
                    return -item.TimeSpent;
                });

                exportDataSvc.registerDataToExport(data, 'Average Time spent in the App');

                self.totalMinutes = averageMinutes();

                self.maxaverageTimeSpent = maxTimeSpent();

                var averageTimeSpent = new RGraph.HBar({

                    id: 'ios-android-windows',
                    data: [
                        self.averageTimeSpent[0].TimeSpent,
                        self.averageTimeSpent[1].TimeSpent,
                        self.averageTimeSpent[2].TimeSpent,
                        self.averageTimeSpent[3].TimeSpent,
                        self.averageTimeSpent[4].TimeSpent,
                        self.averageTimeSpent[5].TimeSpent,
                        self.averageTimeSpent[6].TimeSpent,
                        self.averageTimeSpent[7].TimeSpent,
                        self.averageTimeSpent[8].TimeSpent,
                    ],
                    options: {
                        background: {
                            grid: false
                        },
                        xmax: self.maxaverageTimeSpent,
                        scale: {
                            decimals: 1
                        },
                        colors: {
                            self: ['#15ced0', '#15ced0', '#15ced0','#15ced0','#15ced0','#15ced0','#15ced0','#15ced0'],
                            sequential: true
                        },
                        text: {
                            color: '#fff',
                            size: 8
                        },
                        labels: {
                            self: [self.averageTimeSpent[0].OperatingSystem,
                                   self.averageTimeSpent[1].OperatingSystem,
                                   self.averageTimeSpent[2].OperatingSystem,
                                   self.averageTimeSpent[3].OperatingSystem,
                                   self.averageTimeSpent[4].OperatingSystem,
                                   self.averageTimeSpent[5].OperatingSystem,
                                   self.averageTimeSpent[6].OperatingSystem,
                                   self.averageTimeSpent[7].OperatingSystem,
                                   self.averageTimeSpent[8].OperatingSystem,
                            ]
                        },
                        noxaxis: true,
                        gutter: {
                            left: 100,
                            top: 0
                        },
                        xlabels: false,

                    }
                }).grow();
                formAverageTimeSpent.stop();

            }, function (status) {
                formAverageTimeSpent.stop();
                toastr.error(status);
            });

            function averageMinutes() {
                var totalMinutes = 0;
                for (var i = 0; i < self.averageTimeSpent.length; i++) {
                    totalMinutes = totalMinutes + self.averageTimeSpent[i].TimeSpent;
                }
                var averMin = totalMinutes / self.averageTimeSpent.length;
                var temp = Math.round(averMin * 10);

                return temp / 10;
            }

            function maxTimeSpent() {
                var maxX = self.averageTimeSpent[0].TimeSpent;

                for (var x = 0; x < self.averageTimeSpent.length; x++)
                    if (maxX < self.averageTimeSpent[x].TimeSpent)
                        maxX = self.averageTimeSpent[x].TimeSpent;
                return maxX;
            }
        }]);
});
