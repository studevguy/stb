﻿define(['app',
        'text!./averagetimespent.html',
        './averageTimeSpentCtrl'
], function (app, template) {
    'use strict';
    app.register.directive('averageTimeSpent', [function () {
        return {
            restrict: 'EA',
            template: template,
            controller: 'averageTimeSpentCtrl',
            controllerAs: 'averageTimeSpentCtrl'
        };

    }]);
});