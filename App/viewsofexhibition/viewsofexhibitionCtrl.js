﻿define([
        'app',
        'common/soTheBySvc',
        'common/tinyscrollbarSvc',
        'common/onFinishRender'
], function (app) {
    'use strict';
    app.register.controller('viewsofexhibitionCtrl',
        ['$scope', 'soTheBySvc', 'tinyscrollbarSvc', 'blockUI','exportDataSvc',
            function ($scope, soTheBySvc, tinyscrollbarSvc, blockUI,exportDataSvc) {

                var self = this;
                var formViewsOfExhibitions = blockUI.instances.get('myBlockViewsOfExhibitions');
                var selectedCountryName, countryName;

                formViewsOfExhibitions.start();
                soTheBySvc.getCountries().then(function (data) {
                    self.countries = data;
                    self.countryId = data[0].Id;
                    countryName = data[0].Name;
                    getViewsOfExhibition();
                }, function (status) {
                    formViewsOfExhibitions.stop();
                    toastr.error(status);
                });

                self.loadViewsOfExhibition = function () {
                    formViewsOfExhibitions.start();
                    countryName = _.find(self.countries, function(item){
                        return item.Id == self.countryId;
                    }).Name;
                    self.viewsList = null;
                    getViewsOfExhibition();
                };

                self.tnOptions = {
                    axis: "y",
                    thumbSize: 6,
                    trackSize: 139
                };
                $scope.$on('views-exhibition-country', function () {
                    var $element = '#views-exhibition-country';
                    tinyscrollbarSvc.tinyscrollbar($element, self.tnOptions);
                    tinyscrollbarSvc.update($element);
                });

                var getViewsOfExhibition = function () {
                    soTheBySvc.getViewsOfExhibition(self.countryId).then(function (data) {
                        self.viewsList = data;
                        getSelctedCountryName();
                        exportDataSvc.registerDataToExport(data, 'Views of Exhibitions by Country - '+selectedCountryName,'VE');

                        _.each(self.viewsList, function (item) {
                            item.Views = abbrNum(item.Views, 3);
                        });
                        formViewsOfExhibitions.stop();
                    }, function (status, code) {
                        toastr.error('There is no "view of exhibition" data for '+countryName);
                        formViewsOfExhibitions.stop();
                    });
                };

                // for the Name of table when exporting data
                function getSelctedCountryName(){
                    selectedCountryName = _.find(self.countries, function(item){
                        return item.Id == self.countryId;
                    }).Name;
                }
            }]);
});