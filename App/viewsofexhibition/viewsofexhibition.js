﻿define([
        'app',
        'text!./viewsofexhibition.html',
        './viewsofexhibitionCtrl'
], function (app, viewsofexhibitiontemplate) {
    'use strict';
    app.register.directive('viewsofexhibition', [function () {

        return {
            restrict: 'EA',
            priority: 10,
            template: viewsofexhibitiontemplate,
            controller: 'viewsofexhibitionCtrl',
            controllerAs: 'viewsofexhibitionCtrl'
        };
    }]);
});