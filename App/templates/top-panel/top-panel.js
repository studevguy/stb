define([
    'app',
    'text!./top-panel.html'
], function(
    app,
    topPanelTemplate
){
	'use strict';
	app.register.directive('topPanel',[function(){

	    return {
            restrict: 'EA',
            scope:{
                title: '@'
            },
            template: topPanelTemplate
        }
	}]);
});