﻿define(['angular'], function (angular) {
    'use strict';
    var templateCacheModule = angular.module('autoAppTemplateCache', []);
    templateCacheModule.run(['$templateCache', function ($templateCache) {
        $templateCache.put('blockUITemplate.html', '<div ng-show=\"state.blockCount > 0\" class=\"block-ui-overlay\" ng-class=\"{ \'block-ui-visible\': state.blocking }\"></div> <div ng-show=\"state.blocking\" class=\"block-ui-message-container\"> <img style=\"position:absolute;top:50%;left:50%;width:30px;height:30px;z-index:1200;margin-top:-10px;margin-left:-10px\" src=\"images\/ajax-loader.gif\"\/></div>');
    }]);
})
