﻿define([
    'app',
    'common/soTheBySvc'
], function (app) {
    'use strict';
    app.register.controller('popularSearchCriteriaCtrl', [ 'soTheBySvc', 'blockUI','exportDataSvc',
    function (soTheBySvc, blockUI, exportDataSvc) {
        var self = this;
        var piechart = {};
        var colorArray = ['#036668', '#079E9F', '#15CEE0'];
        var formPopularSearchCriteria = blockUI.instances.get('myBlockPopularSearchCriteria');

        formPopularSearchCriteria.start();
        soTheBySvc.getPopularSearchCriteria().then(function (json) {
            var chartData = soTheBySvc.convertJsonToPopularSearchCriteria(json);
            exportDataSvc.registerDataToExport(json, 'Popular Search Criteria %');

            piechart.data = {
                "cols": [
                    { label: "Keyword", type: "string" },
                    { label: "Hit", type: "number" }
                ],
                "rows": chartData
            };

            piechart.type = "PieChart";

            piechart.options = {
                backgroundColor: {
                    stroke: '#fff',
                    strokeWidth: 0,
                    fill: '#000'
                },
                displayExactValues: true,
                fontSize: 10,
                width: 281,
                height: 135,
                is3D: false,
                chartArea: {
                    left: 10,
                    top: 15,
                    bottom: 15,
                    right: 10,
                    height: "100%",
                    width: "100%"
                },
                tooltip: {
                    trigger: 'none'
                },
                colors: colorArray,
                enableInteractivity: false,
                pieSliceBorderColor: 'black',
                pieSliceText: 'percentage',
                pieSliceTextStyle: {
                    color: 'black'
                },
                legend: {
                    position: 'right',
                    textStyle: {
                        color: 'white'
                    }
                },
                reverseCategories: false,
                pieResidueSliceColor: '#000'
                //sliceVisibilityThreshold: 100/720,
            };

            self.piechart = piechart;
            formPopularSearchCriteria.stop();
        }, function (status) {
            formPopularSearchCriteria.stop();
            toastr.error(status);
        });
    }]);
});

