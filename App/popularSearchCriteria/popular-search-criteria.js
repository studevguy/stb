﻿define([
    'app',
    'text!./popular-search-criteria.html',
    'popularSearchCriteria/popularSearchCriteriaCtrl'
], function (app, popularsearchtemplate) {
    'use strict';
    app.register.directive('popularSearchCriteria', [
        function () {
            return {
                restrict: 'EAC',
                scope: {},
                controller: 'popularSearchCriteriaCtrl',
                controllerAs: 'popularSearchCriteriaCtrl',
                template: popularsearchtemplate
            };
        }]);
});