define(['angular'], function (angular) {
    'use strict';

    angular.module('routeResolver', []).provider('routeResolverSvc', function () {
        var self = this;
        self.PUBLISHED = false;
        self.viewsDirectory = '/app/';

        self.$get = function () {
            return self;
        };
        self.routeConfig = function () {

            var setBaseDirectories = function (viewsDir) {
                self.viewsDirectory = viewsDir;
            };
            var setPulishedMode = function (debugMode) {
                self.PUBLISHED = !debugMode;
            };


            return {
                setViewBaseDirectory: setBaseDirectories,
                setPulishedMode: setPulishedMode,
            };
        }();

        self.route = function (routeConfig) {

            var resolve = function (baseName, template, isRequireAuth) {
                var routeDef = {};
                routeDef.isAuth = false;
                routeDef.isRequireAuth = isRequireAuth;
                //    routeDef.templateUrl = routeConfig.getControllersDirectory() + baseName + '.html';
                routeDef.template = template;
                routeDef.resolve = {
                    load: ['$q', '$rootScope',
                        function ($q, $rootScope) {
                            var modulePath = [baseName.substring(0, baseName.lastIndexOf('/')) + "/index"];
                            var dependencies = [
                                 baseName + 'Ctrl'
                            ];
                            return resolveDependencies($q, $rootScope, dependencies, modulePath);
                        }
                    ]
                };
                routeDef.controller = baseName.substring(baseName.lastIndexOf('/') + 1) + 'Ctrl';

                return routeDef;
            },

            resolveDependencies = function ($q, $rootScope, dependencies, modulePath) {
                var defer = $q.defer();
                if (self.PUBLISHED) {
                    require(modulePath, function () {
                        require(dependencies, function () {
                            defer.resolve();
                            $rootScope.$apply();
                        });
                    });
                } else {
                    require(dependencies, function () {
                        defer.resolve();
                        $rootScope.$apply();
                    });
                }
                return defer.promise;
            };

            return {
                resolve: resolve
            };
        }(self.routeConfig);
    });
});
