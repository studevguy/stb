﻿define([
        'app'
], function (app) {
    'use strict';
    app.register.factory('tinyscrollbarSvc', [
        '$http',
        function ($http) {
            var tinyscrollbar = function ($element, tnOptions) {
                var $scrollbar = $($element);
                $scrollbar.tinyscrollbar({
                    axis: tnOptions.axis || "y",
                    thumbSize: tnOptions.thumbSize || false,
                    trackSize: tnOptions.trackSize || false
                });
            };
            var update = function ($element) {
                var $scrollbar = $($element);
                $scrollbar.data("plugin_tinyscrollbar").update();
            };
            return {
                tinyscrollbar: tinyscrollbar,
                update: update
            };
        }
    ]);
});
