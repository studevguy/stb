﻿define([
        'app'
], function (app) {
    'use strict';
    app.register.factory('soTheBySvc', [
        '$http','$q',
        function ($http, $q) {
            var url ={},baseApiUrl = {
                
                GET_POPULAR_LOTS_BY_SALE_API: 'index.php/api/exhibitions/popularlotsbysales/{0}?startdate={1}&enddate={2}',
                GET_LOT_ORDERS_API: 'index.php/api/exhibitions/lotorders/{0}?startdate={1}&enddate={2}',
                GET_CLICK_EXHIBITION_API: 'index.php/api/click/exhibition/{0}?startdate={1}&enddate={2}',
                GET_AVERAGE_TIME_SPENT_API: 'index.php/api/timeSpent/apps?startdate={1}&enddate={2}',
                GET_NUMBER_VISITS_EXHIBITION_API: 'index.php/api/visits/exhibitions?startdate={1}&enddate={2}',
                GET_POPULAR_DEPARTMENT_API:'index.php/api/popularDepartment?startdate={1}&enddate={2}',
                GET_VIEWS_OF_EXHIBITION_API: 'index.php/api/visits/country/{0}?startdate={1}&enddate={2}',
                GET_TIME_SPENT_GALLERY_API: 'index.php/api/timeSpent/pages?startdate={1}&enddate={2}',
                
                GET_POPULAR_SEARCH_CRITERIA_API: 'index.php/api/popularSearchCriteria?startdate={1}&enddate={2}',
                GET_EXHIBITION_API: 'index.php/api/exhibitions',
                GET_COUNTRY_API: 'index.php/api/countries',
                GET_TOTAL_DOWNLOAD_API: 'index.php/api/downloads?startdate={1}&enddate={2}'
            };

            function setFilter(start, end){
                url = _.clone(baseApiUrl);
                for(var prop in baseApiUrl){
                    url[prop] = baseApiUrl[prop].format('{0}',start, end);
                }

            }
            var getPopularLotsBySale = function (id) {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_POPULAR_LOTS_BY_SALE_API.format(id),
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };
            var getLotOrders = function (id) {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_LOT_ORDERS_API.format(id),
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getClickExhibition = function (id) {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_CLICK_EXHIBITION_API.format(id),
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getTimeSpentGallery = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_TIME_SPENT_GALLERY_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getPopularSearchCriteria = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_POPULAR_SEARCH_CRITERIA_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getAverageTimeSpent = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_AVERAGE_TIME_SPENT_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getNumberVisitsExhibition = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_NUMBER_VISITS_EXHIBITION_API,
                    headers: { 'Content-Type': 'text/plain','Accept': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getPopularDepartment = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_POPULAR_DEPARTMENT_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getViewsOfExhibition = function (id) {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_VIEWS_OF_EXHIBITION_API.format(id),
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var getExhibitions = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_EXHIBITION_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };
            var getCountries = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_COUNTRY_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };
            var getTotalDownloads = function () {
                var deferred = $q.defer();
                $http({
                    method: "get",
                    url: url.GET_TOTAL_DOWNLOAD_API,
                    headers: { 'Content-Type': 'application/json' }
                })
                    .success(function (data) {
                        deferred.resolve(data);
                    }).error(function (data) {
                        deferred.reject(data);
                    });
                return deferred.promise;
            };

            var convertJsonToTimeSpentGallery = function (json) {
                var chartData = [];
                var i = 0, l = json.length;
                for (i, i; i < l; i++) {
                    chartData.push({
                        c: [
                            {
                                v: json[i].PageName
                            },
                            {
                                v: json[i].TimeSpent
                            }
                        ]
                    });
                }
                return chartData;
            };

            var convertJsonToPopularSearchCriteria = function (json) {
                var chartData = [];
                var i = 0, l = json.length;
                for (i, i; i < l; i++) {
                    chartData.push({
                        c: [
                            {
                                v: json[i].Keyword
                            },
                            {
                                v: json[i].Hit
                            }
                        ]
                    });
                }
                return chartData;
            };

            var convertToPercentage = function (value, total) {
                var result = value / total * 100;
                return result;
            };

            var countUrl = function (){
                var temp = _.keys(url);
                return temp.length;
            };

            return {
                getPopularLotsBySale: getPopularLotsBySale,
                getLotOrders: getLotOrders,
                getClickExhibition: getClickExhibition,
                getTimeSpentGallery: getTimeSpentGallery,
                convertJsonToTimeSpentGallery: convertJsonToTimeSpentGallery,
                convertJsonToPopularSearchCriteria: convertJsonToPopularSearchCriteria,
                getPopularSearchCriteria: getPopularSearchCriteria,
                getAverageTimeSpent: getAverageTimeSpent,
                getNumberVisitsExhibition: getNumberVisitsExhibition,
                getPopularDepartment: getPopularDepartment,
                getViewsOfExhibition: getViewsOfExhibition,
                convertToPercentage: convertToPercentage,
                getExhibitions: getExhibitions,
                getCountries: getCountries,
                getTotalDownloads: getTotalDownloads,
                setFilter: setFilter,
                countUrl: countUrl
            };
        }
    ]);
});
