﻿define([
    'app'
], function (app) {
    app.register.directive('selectPicker', ['$window', function ($window) {
        return {
            restrict: 'EA',
            scope: {},
            link: function ($scope, $element) {
                var w = angular.element($window);
                setTimeout(function() {
                    $($element).selectpicker({
                        size: 10,
                        width: '100%',
                    });
                }, 0);
            }
        };
    }]);
});