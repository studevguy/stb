//export data to csv file

define([
    'app'
], function(
    app
){
	'use strict';
	app.register.service('exportDataSvc',[function(){

	    var self  = this,
            STORINGPLACE = {
                clickExhibition: 'CE',
                viewsOfExhibition: 'VE'
        };
        self.data = [];
        self.clickExhibitionData = [];
        self.viewsOfExhibitionData = [];

        self.registerDataToExport = function(data, name, storingPlace){
            var obj;

            //select variable to store data
            switch(storingPlace) {
                case STORINGPLACE.clickExhibition:
                    obj = self.clickExhibitionData = [];
                    break;
                case STORINGPLACE.viewsOfExhibition:
                    obj = self.viewsOfExhibitionData = [];
                    break;
                default:
                    obj =self.data;
            }

            //insert a space line before each data group
            var spaceLine = {
                0:""
            };
            obj.push(spaceLine);

            // store name of data
            var tableNameObject = {
                0:  name
            };
            obj.push(tableNameObject);

            //store properties as a field of table
            var temp = data.length > 1 ? data[0] : data;
            var propertyObject ={};
            for (var prop in temp){
                propertyObject[prop] = prop;
            }
            obj.push(propertyObject);

            //store data
            if (data.length>1){
                _.each(data, function(item){
                    obj.push(item);
                } )
            }
            else{
                obj.push(data);
            }
        };
        self.getData = function(){
            return _.union(self.data, self.clickExhibitionData, self.viewsOfExhibitionData);
        }
	}]);
});