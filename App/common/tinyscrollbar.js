﻿define([
    'app'
], function (app) {
    app.register.directive('tinyscrollbar', ['$window', function ($window) {
        return {
            restrict: 'EA',
            priority: 1,
            scope: {
                tnAxis: '=',
                tnThumbSize: '=',
                tnTrackSize: '=',
                tnElement: '=',
                updateData: '='
            },
            link: function ($scope, $element) {
                var w = angular.element($window);
                var $scrollbar = $($element);
                setTimeout(function () {
                    $scrollbar.tinyscrollbar({
                        axis: $scope.tnAxis || "y",
                        thumbSize: $scope.tnThumbSize || false,
                        trackSize: $scope.tnTrackSize || false
                    });
                }, 0);
            }
        };
    }]);
});