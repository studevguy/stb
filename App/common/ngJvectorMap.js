﻿define([
    'app'
], function (app) {
    app.register.directive('ngJvectorMap', [ function () {
        return {
            restrict: 'EA',
            link: function ($scope, $elm) {
                $($elm).vectorMap({
                    onRegionClick: function(e, code){
                        console.log(e);
                        console.log(code);
                    },
                    backgroundColor: "#000000",
                    focusOn:{
                        x:0,
                        y:0.38,
                        scale:1.9
                    },
                    regionStyle:{
                        initial: {
                            fill: 'white',
                            "fill-opacity": 1,
                            stroke: 'none',
                            "stroke-width": 0,
                            "stroke-opacity": 1
                        },
                        hover: {
                            "fill-opacity": 0.8,
                            fill: '#15ced0'
                        },
                        selected: {
                            fill: 'yellow'
                        },
                        selectedHover: {
                        }
                    }
                });
            }
        };
    }]);
});