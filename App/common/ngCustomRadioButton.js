define([
    'app'
], function(
    app
){
	'use strict';
	app.register.directive('ngCustomRadioButton',[function(){

	    return {
            restrict: 'AE',
            link: function(scope, elm){
                $(elm).screwDefaultButtons({
                    image: "url(images/custom-radio.png)",
                    width:	 23,
                    height:	 23
                })
            }
        }
	}]);
});