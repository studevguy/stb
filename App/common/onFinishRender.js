﻿define([
    'app'
], function (app) {
    app.register.directive('onFinishRender', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                scope.$watch(element,function () {
                    if (scope.$last === true) {
                        element.ready(function () {
                            scope.$emit(attr.onFinishRender);
                        });
                    }
                });
                
            }
        };
    });
});