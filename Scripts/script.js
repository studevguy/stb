﻿$(document).ready(function () {
    $("#mostpopularfirst").tinyscrollbar({
        axis: "x",
        thumbSize: 14
    });
    $("#number-visits-revisits").tinyscrollbar({
        thumbSize: 6,
        axis: 'y'
    });
    $("#views-exhibition-country").tinyscrollbar({
        thumbSize: 6,
        axis: 'y',
        trackSize:95
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $("#lotorder").tinyscrollbar({
            axis: "x",
            thumbSize: 14
        });
    });



});