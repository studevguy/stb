﻿//$(document).ready(function() {
//    var hbar = new RGraph.HBar({
//        id: 'ios-android-windows',
//        data: [100, 90, 80],
//        options: {
//            background: {
//                grid: false
//            },
//            xmax: 100,
//            scale: {
//                decimals: 1
//            },
//            colors: {
//                self: ['#15ced0', '#15ced0', '#15ced0'],
//                sequential: true
//            },
//            text: {
//                color: '#fff'
//            },
//            labels: {
//                self: ['IOS', 'Android', 'Windows'],
//                //above: {
//                //    self: true,
//                //    decimals: 0


//                //}
//            },
//            noxaxis: true,
//            gutter: {
//                left: 60,
//                top: 0
//            },
//            xlabels: false
//        }
//    }).grow();
//});
//Impressionist & Modern Art
$(document).ready(function() {
    var hbar = new RGraph.HBar({
        id: 'item-Impressionist',
        data: [4.2],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            labels: {
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-Contemporary
$(document).ready(function() {
    var hbar = new RGraph.HBar({
        id: 'item-Contemporary',
        data: [3.9],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-painting
$(document).ready(function () {
    var hbar = new RGraph.HBar({
        id: 'item-painting',
        data: [2.7],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-century
$(document).ready(function () {
    var hbar = new RGraph.HBar({
        id: 'item-century',
        data: [2.6],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-prints
$(document).ready(function () {
    var hbar = new RGraph.HBar({
        id: 'item-prints',
        data: [1.3],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-photography
$(document).ready(function () {
    var hbar = new RGraph.HBar({
        id: 'item-photography',
        data: [1.1],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-art
$(document).ready(function () {
    var hbar = new RGraph.HBar({
        id: 'item-art',
        data: [0.7],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});
//item-britishpainting
$(document).ready(function () {
    var hbar = new RGraph.HBar({
        id: 'item-britishpainting',
        data: [0.2],
        options: {
            background: {
                grid: false
            },
            xmax: 4.2,
            scale: {
                decimals: 1
            },
            colors: {
                self: ['#15ced0'],
                sequential: true
            },
            text: {
                color: '#fff'
            },
            noxaxis: true,
            gutter: {
                left: 0,
                top: 0
            },
            xlabels: false
        }
    }).grow();
});

